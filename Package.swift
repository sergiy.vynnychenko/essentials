// swift-tools-version:5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Essentials",
    platforms: [.macOS(.v12), .iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "Essentials", type: .dynamic, targets: ["Essentials"]),
        .library(name: "EssentialsStatic", type: .static, targets: ["Essentials"]),
        .library(name: "EssentialsTesting", type: .static, targets: ["EssentialsTesting"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(name: "Essentials", dependencies: []),
        .target(name: "EssentialsTesting", dependencies: ["Essentials"]),
        .testTarget(name: "EssentialsTests", dependencies: ["Essentials", "EssentialsTesting"]),
    ]
)
