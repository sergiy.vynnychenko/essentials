import XCTest
import Essentials

public extension Result {
    func verify(_ topic: String? = nil) -> Self {
        onSuccess {
            topic?.print(success: $0)
        }.onFailure {
            (topic ?? "verify").print(failure: $0)
            XCTAssert(false)
        }
    }
    
    @discardableResult
    func shouldSucceed(_ topic: String? = nil) -> Success? {
        onSuccess {
            topic?.print(success: $0)
        }.onFailure {
            (topic ?? "shouldSucceed").print(failure: $0)
            XCTAssert(false)
        }
        return maybeSuccess
    }
        
    @discardableResult
    func shouldFail(_ topic: String? = nil) -> Success? {
        onSuccess {
            (topic ?? "shouldFail").print(success: $0, asci: true)
            XCTAssert(false)
        }.onFailure {
            topic?.print(failure: $0, asci: false)
        }
        return maybeSuccess
    }
    
    @discardableResult
    func assertEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
        onSuccess {
            if to == $0 {
                topic?.print(success: "\(to) == \($0)")
            } else {
                (topic ?? "Assert Equal").print(failure: WTF("\(to) != \($0)"))
            }
            XCTAssert(to == $0)
        }.onFailure {
            (topic ?? "Assert Equal").print(failure: $0)
            XCTAssert(false)
        }
        
        return maybeSuccess
    }
    
    @discardableResult
    func assertNotEqual(to: Success, _ topic: String? = nil) -> Success? where Success: Equatable {
        onSuccess {
            if to != $0 {
                topic?.print(success: "\(to) != \($0)")
            } else {
                (topic ?? "Assert NOT Equal").print(failure: WTF("\(to) == \($0)"))
            }
            XCTAssert(to != $0)
        }.onFailure {
            topic?.print(failure: $0)
            XCTAssert(false)
        }
        
        return maybeSuccess
    }
    
    @discardableResult
    func assertBlock(_ topic: String? = nil, block: (Success) -> Bool) -> Success? {
        onSuccess {
            if block($0) {
                topic?.print(success: $0)
            } else {
                topic?.print(failure: WTF("assertion failed for \($0)"))
                XCTAssert(false, topic ?? "")
            }
        }.onFailure {
            topic?.print(failure: $0)
            XCTAssert(false)
        }
        
        return maybeSuccess
    }
}

private extension Result {
    @discardableResult
    func onSuccess(_ block: (Success)->Void) -> Self {
        switch self {
        case .success(let success): block(success)
        default: break
        }
        return self
    }
    
    @discardableResult
    func onFailure(_ block: (Error)->Void) -> Self {
        switch self {
        case .failure(let error): block(error)
        default: break
        }
        return self
    }
}

extension String {
    func print<T>(success: T, asci: Bool = false) {
        Swift.print("+-----------------------------------------------)")
        Swift.print("+ SUCCEEDED: \(self)")
        Swift.print("+ WITH:      \(success)")
        Swift.print("+-----------------------------------------------)")
        if asci {
            Swift.print(bug)
        }
    }

    func print(failure: Error, asci: Bool = true) {
        let nsError = failure as NSError
        Swift.print("+-----------------------------------------------)")
        Swift.print("+ FAILED:    \(self)")
        Swift.print("+ WITH:      \(failure.localizedDescription)")
        if let reason = nsError.localizedFailureReason {
        Swift.print("+ REASON:    \(reason)")
        }
        Swift.print("+ CODE:      \(nsError.code)")
        Swift.print("+-----------------------------------------------)")
        if asci {
            Swift.print(bug)
        }
    }
}

let bug = #"""
    \ /
    oVo
\___XXX___/
 __XXXXX__
/__XXXXX__\
/   XXX   \
     V
"""#
