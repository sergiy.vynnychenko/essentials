import Foundation

public extension Date {
    func asString(_ template: String? = nil) -> String {
        if let template = template {
            let df = DateFormatter.with(template: template)
            
            return df.string(from: self)
        }
        else {
            return globalDateFormatter.string(from: self)
        }
    }
    
    static func fromString(strDate: String, _ template: String,
                           locale: Locale = Locale(identifier: "en_US"),
                           timeZoneFix: TimeZone? = TimeZone(abbreviation: "GMT+0:00") ) -> Date?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = template
        dateFormatter.locale = locale
        dateFormatter.timeZone = timeZoneFix
        return dateFormatter.date(from: strDate)
    }
}

fileprivate let globalDateFormatter: DateFormatter = DateFormatter.with(template: "y-M-dd")

public extension DateFormatter {
    static func with(template: String ) -> DateFormatter {
        let df = DateFormatter()
        df.dateFormat = template
        return df
    }
}

public extension Date {
    func plus(seconds: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
    
    func plus(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func plus(mins: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: mins, to: self)!
    }
    
    func plus(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
}

public extension Date {
    func removingTimeStamp() -> Date {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.calendar, .timeZone, .era, .day, .year, .month], from: self))
        else { fatalError("Failed to strip time from Date object") }
        
        return date
    }
}

public extension Date {
    static var todayBeginning: Date {
        return Date().removingTimeStamp()
    }
    
    static var tomorrow: Date {
        return todayBeginning
            .adding(.day, value: 1)
    }
}

public extension Date {
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}
