import Foundation

public class MutexRecursiveLock : LockingProtocol {
    private var _lock = pthread_mutex_t()
    
    public init() {
        var attr = pthread_mutexattr_t()
        pthread_mutexattr_init(&attr)
        pthread_mutexattr_settype(&attr, Int32(PTHREAD_MUTEX_RECURSIVE))
        pthread_mutex_init(&_lock, &attr)
    }
    
    deinit {
        pthread_mutex_destroy(&_lock)
    }
    
    @inline(__always) public func locked<ReturnValue>(_ f: () throws -> ReturnValue) rethrows -> ReturnValue {
        pthread_mutex_lock(&_lock)
        defer { pthread_mutex_unlock(&_lock) }
        return try f()
    }
}
