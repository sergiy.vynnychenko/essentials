
import Foundation

public extension Thread {
    var dbgName: String {
        if let currentOperationQueue = OperationQueue.current?.name {
            
            if currentOperationQueue.contains("OperationQueue") {
                return currentOperationQueue
            } else {
                return "OperationQueue: \(currentOperationQueue)"
            }
            
        } else if let underlyingDispatchQueue = OperationQueue.current?.underlyingQueue?.label {
            
            if underlyingDispatchQueue.contains("DispatchQueue") {
                return underlyingDispatchQueue
            } else {
                return "DispatchQueue: \(underlyingDispatchQueue)"
            }
            
        } else {
            let name = __dispatch_queue_get_label(nil)
            return String(cString: name, encoding: .utf8) ?? Thread.current.description
        }
    }
}
