import Foundation

protocol LockingProtocol {
    func locked<ReturnValue>(_ f: () throws -> ReturnValue) rethrows -> ReturnValue
}

public final class UnfairLock : LockingProtocol {
    private var _lock: UnsafeMutablePointer<os_unfair_lock>
    
    public init() {
        _lock = UnsafeMutablePointer<os_unfair_lock>.allocate(capacity: 1)
        _lock.initialize(to: os_unfair_lock())
    }
    
    deinit {
        _lock.deallocate()
    }
    
    @inline(__always) public func locked<ReturnValue>(_ f: () throws -> ReturnValue) rethrows -> ReturnValue {
        os_unfair_lock_lock(_lock)
        defer { os_unfair_lock_unlock(_lock) }
        return try f()
    }
}
