public struct LockedVar<T> {
    fileprivate let locker = UnfairLock()
    fileprivate var variable : T
    
    @available(*, deprecated, message: "unsafe access, delete me")
    public var wrapped: T { return variable }
    
    public init(_ variable: T) {
        self.variable = variable
    }
    
    public mutating func access<O>(_ block: (inout T)->O) -> O {
        locker.locked {
            block(&variable)
        }
    }
    
    public var read : T { locker.locked { variable } }
    
    public func read<O>(_ block: (T)->O) -> O {
        locker.locked {
            block(variable)
        }
    }
    
    public func read<O>(_ keyPath: KeyPath<T,O>) -> O {
        locker.locked {
            variable[keyPath: keyPath]
        }
    }
}

public extension LockedVar {
    mutating func factory<Key,Value>(key: Key, block: (Key)->Value) -> Value where T == Dictionary<Key,Value> {
        self.access { $0.item(key: key, block) }
    }
}

public extension LockedVar  {
    subscript<T2> (_ index: T.Index) -> T.Element? where T == Array<T2> {
        get {
            self.locker.locked {
                variable.indices.contains(index) ? variable[index] : nil
            }
        }
        mutating set {
            self.locker.locked {
                if variable.indices.contains(index), let value = newValue {
                    variable[index] = value
                }
            }
        }
    }
    
    mutating func item<Key:Hashable,Value>(key: Key, _ block: (Key)->Value) -> Value where T == Dictionary<Key,Value> {
        self.access { dic in
            dic.item(key: key, block)
        }
    }

    
    subscript<Key:Hashable,Value> (_ key: Key) -> Value? where T == Dictionary<Key,Value> {
        get {
            self.locker.locked { variable[key] }
        }
        mutating set {
            self.locker.locked {
                variable[key] = newValue
            }
        }
    }
    
    func contains<Key:Hashable,Value>(key: Key) -> Bool where T == Dictionary<Key,Value> {
        self.read { $0.keys.contains(key) }
    }
    

}

public extension LockedVar  {
    func contains<Value>(_ value: Value) -> Bool where T == Set<Value> {
        self.read { $0.contains(value) }
    }
    
    @discardableResult
    mutating func insert<Value>(_ value: Value) -> (inserted: Bool, memberAfterInsert: T.Element) where T == Set<Value> {
        self.access { $0.insert(value) }
    }
    
    @discardableResult
    mutating func remove<Value>(_ value: Value) -> T.Element? where T == Set<Value> {
        self.access { $0.remove(value) }
    }
}

prefix operator *
public prefix func *<T>(right: LockedVar<T>) -> T {
    right.locker.locked { right.variable }
}

public func *= <T>( left: inout T, right: LockedVar<T>) {
    left = *right
}

public func *= <T>( left: inout LockedVar<T>, right: T) {
    left.access {
        $0 = right
    }
}







public enum LockType {
    case unfair
    case recursive
}

/// using: @WrappedProducer var variable = "String Value"
/// variable can be used as usual
/// channel can be accessed as $variable
@propertyWrapper public struct Locked<Value> { // SLOW
    private var lock : LockingProtocol
    public var wrappedValue : Value {
        @inline(__always) get {  projectedValue } 
        @inline(__always) set {  self.projectedValue = newValue }
    }
    
    /// The property that can be accessed with the `$` syntax and allows access to the `Channel`
    public var projectedValue: Value
    /// Initialize the storage of the Published property as well as the corresponding `Publisher`.
    public init(wrappedValue: Value, _ type: LockType) {
        //self.wrappedValue = wrappedValue
        self.projectedValue = wrappedValue
        switch type {
        case .unfair:
            self.lock = UnfairLock()
        case .recursive:
            self.lock = MutexRecursiveLock()
        }
    }
}
