public protocol ThreadSafeAccessStruct {
    var lock : UnfairLock { get }
}

extension ThreadSafeAccessStruct {
    func findFirst<T>(in: KeyPath<Self,Array<T>>, block: (T)->Bool) -> T? {
        self[keyPath: `in`].first(where: block)
    }
}

public extension ThreadSafeAccessStruct {
    func read<T>(from keyPath: KeyPath<Self,T>) -> T {
        lock.locked {
            self[keyPath: keyPath]
        }
    }
    
    func read<T,T2>(from keyPath: KeyPath<Self,T>, block: (T)->(T2)) -> T2 {
        block(lock.locked { self[keyPath: keyPath]})
    }
    
    func read<T1,T2>(from keyPath: KeyPath<Self,T1>, at path: KeyPath<T1,T2>) -> T2 {
        lock.locked { self[keyPath: keyPath][keyPath: path] }
    }
    
    mutating func cache<Key,Value>(keyPath: WritableKeyPath<Self,R<Value>.LazyDic<Key>>, key: Key) -> R<Value> {
        let lazyDic = lock.locked { self[keyPath: keyPath] }
        if let value = lazyDic.dic[key] {
            return .success(value)
        }
        
        return lazyDic.getter(key).onSuccess { value in
            lock.locked {
                self[keyPath: keyPath].dic[key] = value
            }
        }
    }
    
    mutating func cache<Key,Value>(keyPath: WritableKeyPath<Self,LazyDic<Key,Value>>, key: Key) -> Value {
        let lazyDic = lock.locked { self[keyPath: keyPath] }
        if let value = lazyDic.dic[key] {
            return value
        }
                
        let value = lazyDic.getter(key)
        lock.locked {
            self[keyPath: keyPath].dic[key] = value
        }
        
        return value
    }
    
    mutating func map<T>(keyPath: WritableKeyPath<Self,T>, block: (T)->(T)) {
        let value = lock.locked { self[keyPath: keyPath] }
        let result = block(value)
        lock.locked {
            self[keyPath: keyPath] = result
        }
    }
    
    mutating func write<T,T2>(keyPath: WritableKeyPath<Self,T>, block: (inout T)->(T2)) -> T2 {
        lock.locked {
            block(&self[keyPath: keyPath])
        }
    }
}
