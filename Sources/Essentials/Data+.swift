import Foundation
import CryptoKit

@available(iOS 11.0, *)
public extension Data {
    func asString() -> R<String> {
        if let str = String(data: self, encoding: .utf8) {
            return .success(str)
        } else {
            return .failure(WTF("can't convert Data to string"))
        }
    }
    
    func write(url: URL) -> R<Data> {
        do {
            try write(to: url )
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    @available(macOS 10.13, *)
    static func archived(withRootObject object: Any, requiringSecureCoding requiresSecureCoding: Bool) -> R<Data> {
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject:object,requiringSecureCoding:requiresSecureCoding)
            return .success(data)
        } catch {
            return .failure(error)
        }
    }
}
