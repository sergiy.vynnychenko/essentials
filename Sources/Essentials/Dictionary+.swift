import Foundation

public extension Dictionary where Value : Equatable {
    func diff(with other: [Key:Value]) -> Set<Key> {
        var result = Set<Key>()
        
        for item in self {
            if other[item.key] != item.value {
                result.insert(item.key)
            }
        }
        
        for item in other {
            if self[item.key] != item.value {
                result.insert(item.key)
            }
        }
        
        return result
    }
}

public extension Dictionary where Key: Encodable, Value: Encodable {
    func asJSONStr() -> String? {
        let encoder = JSONEncoder()
        if let jsonData = try? encoder.encode(self) {
            if let jsonString = String(data: jsonData, encoding: .utf8) {
                return jsonString
            }
        }
        
        return nil
    }
    
    func asJSONStrR() -> R<String?> {
        let encoder = JSONEncoder()
        
        return Result { try encoder.encode(self) }
            .map{ jsonData in
                return String(data: jsonData, encoding: .utf8)
            }
    }
}

public extension Dictionary {
    init<S: Sequence>(_ keys: S, withVal defaultVal: Value) where S.Element == Key {
        self = Dictionary( uniqueKeysWithValues: zip(keys, AnyIterator { defaultVal }) )
    }
}

public extension Dictionary {
    var sizeInBytes: Int {
        if let type_ = Value.self as? AnyClass {
            return class_getInstanceSize(type_) * self.count
        }
        
        return 0
    }
}

public extension Dictionary {
    mutating func remove(key: Key) {
        guard let idx = self.index(forKey: key) else { return }
        
        self.remove(at: idx)
    }
}

public extension Dictionary {
    func mapValues<T>(_ transform: (Value) -> T) -> [Key: T] {
        var newDict = [Key: T]()
        for (key, value) in self {
            newDict[key] = transform(value)
        }
        return newDict
    }
}

public extension Dictionary {
    mutating func item(key: Key, _ block: (Key)->Value) -> Value {
        if let value = self[key] {
            return value
        }
        let value = block(key)
        self[key] = value
        return value
    }
}
