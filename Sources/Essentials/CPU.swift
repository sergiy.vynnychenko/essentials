#if os(macOS)

import Foundation

let CPU_ARCH_MASK   : Int32        = 0xff      // mask for architecture bits

let CPU_TYPE_X86    : Int32       = cpu_type_t(7)
let CPU_TYPE_ARM    : Int32       = cpu_type_t(12)

public enum CPUType {
    case x86
    case arm
    case undefined
}

public struct CPU {
    public static var architecture : CPUType {
        let _type = _CPUType()
        if _type == -1 {
            return .undefined
        }
        
        let cpu_arch   : Int32         = _type & CPU_ARCH_MASK
        
        switch cpu_arch {
        case CPU_TYPE_ARM: return .arm
        case CPU_TYPE_X86: return .x86
        default:
            return .undefined
        }
    }
}

private func _CPUType() -> Int32 {
    var cputype = UInt32(0)
    var size = cputype.bitWidth/UInt8.bitWidth
    
    let result = sysctlbyname("hw.cputype", &cputype, &size, nil, 0)
    if result == -1 {
        if (errno == ENOENT){
            return 0
        }
        return -1
    }
    return Int32(cputype)
}

#endif
