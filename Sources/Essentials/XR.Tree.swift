public extension XR {
    struct Tree {
        public private(set) var items     = Swift.Set<String>()
        public private(set) var childrenOf   = [String:Swift.Set<String>]()
        public private(set) var parentOf     = [String:String]()
        
        public init() {
        }
        
        // Remove notifications not supported yet
        mutating public func update(parent: String, children newChildren: [String]) -> Update {
            let _removed = [String]()
            
            let _inserted = self.add(children: newChildren, parent: parent)
            
            return Update(inserted: Array(_inserted), removed: _removed)
        }
        
        mutating func add(children: [String], parent: String) -> Swift.Set<String> {
            let oldItems = self.items
            
            if !self.items.contains(parent) {
                self.items.insert(parent)
            }
            
            for item in children {
                if !self.items.contains(item) {
                    if childrenOf[parent] == nil {
                        childrenOf[parent] = Swift.Set<String>()
                    }
                    childrenOf[parent]?.insert(item)
                    parentOf[item] = parent
                    self.items.insert(item)
                }
            }
            
            return oldItems.symmetricDifference(items)
        }
        
        public struct Update {
            public let inserted : [String]
            public let removed : [String]
            
            public init(inserted: [String], removed: [String]) {
                self.inserted = inserted
                self.removed = removed
            }
        }
        
        public func allChildren(parent: String) -> Swift.Set<String> {
            if let children = self.childrenOf[parent] {
                return children.union(children.flatMap { allChildren(parent: $0) })
            } else {
                return []
            }
        }
    }
}

private func _diff(old: Set<String>, new: Set<String>) -> XR.Tree.Update {
    var inserted = [String]()
    var removed = [String]()
    
    let diff = old.symmetricDifference(new)
    
    for child in diff {
        if !old.contains(child) {
            inserted.append(child)
        } else {
            removed.append(child)
        }
    }
    
    return XR.Tree.Update(inserted: inserted, removed: removed)
}

public func +(left: XR.Tree.Update, right: XR.Tree.Update) -> XR.Tree.Update { // 1
    XR.Tree.Update(inserted: left.inserted + right.inserted, removed: left.removed + right.removed)
}
