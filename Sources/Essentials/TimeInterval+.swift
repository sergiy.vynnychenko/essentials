
import Foundation

public extension TimeInterval {
    init(days: Int = 0, hrs: Int = 0, mins: Int = 0, sec: Int = 0) {
        let timeInSec = (sec) + (mins * 60) + (hrs * 60 * 60) + (days * 24 * 60 * 60)
        self.init(timeInSec)
    }
}

public extension TimeInterval {
    var msVal: Double {
        return truncatingRemainder(dividingBy: 1) * 1000
    }
    
    var secVal: Int {
        return Int(self) % 60
    }
    
    var minsVal: Int {
        return (Int(self) / 60 ) % 60
    }
    
    /// Int value of days. Rounded
    var daysTotal: Int {
        Int(self) / 86400
    }
    
    var hoursTotal: Int {
        return Int(self) / 3600
    }
    
    var secTotal: Int {
        Int(self)
    }
    
    var minsTotal: Int {
        Int(self)/60
    }
    
    var minsTotalAbs: Int {
        abs( Int(self)/60 )
    }
    
    var secTotalAbs: Int {
        abs( Int(self) )
    }
}

public extension TimeInterval{
    func asStr() -> String {
        var formatString: String
        
        if hoursTotal == 0 {
            if (minsVal < 10) {
                formatString = "%2d:%0.2d"
            } else {
                formatString = "%0.2d:%0.2d"
            }
            
            return String(format: formatString, minsVal, secVal)
        } else {
            formatString = "%2d:%0.2d:%0.2d"
            return String(format: formatString, hoursTotal, minsVal, secVal)
        }
    }
}
