import Foundation

//public struct WTF : Error {
//    var localizedDescription : String
//
//    public init(_ desc: String) {
//        localizedDescription = desc
//    }
//}

public func WTF(_ msg: String, code: Int = 0) -> Error {
    NSError(code: code, message: msg)
}

internal extension NSError {
    convenience init(code: Int, message: String) {
        let userInfo: [String: String] = [NSLocalizedDescriptionKey:message]
        self.init(domain: "FTW", code: code, userInfo: userInfo)
    }
}

public extension Result {
    static func wtf(_ m: String, code: Int = 0) -> Result<Success,Error> {
        return .failure(WTF(m, code: code))
    }
}

public extension Result {
    static var notImplemented : Result<Success,Error> {
        return .wtf("something not implemented")
    }
    
    static func notImplemented(_ topic: String) -> Result<Success,Error> {
        return .wtf("\(topic) not implemented")
    }
}

public extension Error {
    var descriptionTitle : String {
        var result = "\((self as NSError).code)"
        if let domain = __domain {
            result += "\(domain)"
        }
        
        result += " : " + self.localizedDescription
        
        return result
    }
    
    var detailedDescription_MultiLine : String {
        var result = ""
        
        result += "ERROR : " + self.localizedDescription
        result += "\nCODE   : \((self as NSError).code)"
        
        if let domain = __domain {
            result += "\(domain)"
        }
        
        if let reason = __reason {
            result += "\n\(reason)"
        }
        
        if let userInfo = __userInfo {
            result += "\n\(userInfo)"
        }
        
        return result
    }
    
    var detailedDescription : String {
        var result = __code + " " + self.localizedDescription
        
        if let reason = __reason {
            result += " | \(reason)"
        }
        
        if let domain = __domain {
            result += " | \(domain)"
        }
        
        if let userInfo = __userInfo {
            result += " | \(userInfo)"
        }
        
        return result
    }
    
    var code : Int {
        (self as NSError).code
    }
}

extension Error {
    public var __reason : String? {
        let nsError = self as NSError
        if let reason = nsError.localizedFailureReason {
            return "REASON: \(reason)"
        }
        return nil
    }
    
    var __code : String {
        let nsError = self as NSError
        return "[code: \(nsError.code)]"
    }
    
    var __domain : String? {
        let nsError = self as NSError
        
        if nsError.domain != "" {
            return " @ \(nsError.domain)"
        }
        return nil
    }
    
    public var __userInfo : String? {
        let nsError = self as NSError
        
        var userInfo = nsError.userInfo
        userInfo[NSLocalizedDescriptionKey] = nil
        userInfo[NSLocalizedFailureReasonErrorKey] = nil
        if !userInfo.isEmpty {
            return "UserInfo: \(userInfo)"
        }
        
        return nil
    }
}
