public extension Optional {
    func asResult<T>(_ transform: (Wrapped) -> Result<T, Error>) -> Result<T, Error> {
        if let val = self {
            return transform(val)
        } else {
            return .failure(WTF("nil for optional value of type <\(Wrapped.self)>"))
        }
    }
    
    func asResult<T>(_ transform: (Wrapped) -> T) -> Result<T, Error> {
        if let val = self {
            return .success(transform(val))
        } else {
            return .failure(WTF("nil for optional value of type <\(Wrapped.self)>"))
        }
    }
    
    var asNonOptional : R<Wrapped> {
        if let val = self {
            return .success(val)
        } else {
            return .failure(WTF("nil for optional value of type <\(Wrapped.self)>"))
        }
    }
    
    func asNonOptional( _ pointOfFailure: String) -> R<Wrapped> {
        if let val = self {
            return .success(val)
        } else {
            return .failure(WTF("[\(pointOfFailure)] nil for optional value of type <\(Wrapped.self)>"))
        }
    }
}
