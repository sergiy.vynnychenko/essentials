import Foundation

public struct ResultCatch<E,T> where E : Hashable {
    public let errors  : [E:Error]
    public let results : T
}

public extension Array {
    func reduce<A>(_ accum: A, _ block: (A, Element) -> Result<A, Error>) -> Result<A, Error> {
        var a = accum
        for item in self {
            switch block(a, item) {
            case .success(let success):
                a = success
            case .failure(let error):
                return .failure(error)
            }
        }
        return .success(a)
    }
    
    func reduceCatch<A>(_ accum: A, _ block: (A, Element) -> Result<A, Error>) -> ResultCatch<Element, A>  where Element : Hashable {
        var a = accum
        var errors = [Element:Error]()
        for item in self {
            switch block(a, item) {
            case .success(let success):
                a = success
            case .failure(let error):
                errors[item] = error
            }
        }
        return ResultCatch(errors: errors, results: a)
    }
    
/// the non optimized version of flatMap would look like this:
///
//    func flatMap<T>(_ transform: @escaping (Element) -> Result<T,Error>) -> Result<[T],Error> {
//        return self.reduce([T]()) { array, next in
//            return transform(next)
//                .map { transformedNext in array + transformedNext }
//        }
//    }
    
    func flatMap<T>(_ transform: @escaping (Element) -> Result<T,Error>) -> Result<[T],Error> {
        return self.reduce(ArrayWrapper(self.count)) { array, next in
            return transform(next)
                .map { transformedNext in array + transformedNext }
        }.map { $0.array }
    }
    
    func flatMapCatch<T>(_ transform: @escaping (Element) -> Result<T,Error>) -> ResultCatch<Element, [T]> where Element : Hashable {
        let r =  self.reduceCatch(ArrayWrapper(self.count)) { array, next in
            return transform(next)
                .map { transformedNext in array + transformedNext }
        }//.map {
        return ResultCatch(errors: r.errors, results: r.results.array)
    }
    
    
}

public extension Array {
    static func |<Transformed>(left: Self, right: (Element)->Transformed) -> Array<Transformed> { // 2
        return left.map { right($0) }
    }
    
    static func |<Transformed>(left: Self, right: (Element)->Array<Transformed>) -> Array<Transformed> { // 2
        return left.flatMap { right($0) }
    }
    
    static func |<Transformed>(left: Self, right: @escaping (Element)->Result<Transformed, Error>) -> Result<[Transformed], Error> { // 2
        return left.flatMap { right($0) }
    }
}

class ArrayWrapper<T> {
    var array : [T]
    init(_ reserve: Int) {
        array = [T]()
        array.reserveCapacity(reserve)
    }
    
    static func + (left: ArrayWrapper<T>, right: T) -> ArrayWrapper<T> {
        left.array.append(right)
        return left
    }
    
    static func + (left: ArrayWrapper<T>, right: [T]) -> ArrayWrapper<T> {
        left.array.append(contentsOf: right)
        return left
    }
}
