import Foundation

public enum TmpDir {
    case userRoot
    case userUnique
    case systemRoot
    case systemUnique
}

@available(iOS 13.4, *)
public extension URL {
    func moveFile(to: URL) -> R<()> {
        do {
            try FileManager.default.moveItem(at: self, to: to)
            return .success(())
        } catch {
            return .failure(error)
        }
    }
    
    func copy(to: URL, replace: Bool = false) -> R<()> {
        guard self.exists else { return .wtf("copy URL source doesn't exists") }
        
        if replace {
            _ = to.rm()
        }
        
        do {
            try FileManager.default.copyItem(at: self, to: to)
            return .success(())
        } catch {
            return .failure(error)
        }
    }
    
    var isDirExist: Bool {
        var isDirectory = ObjCBool(true)
        let exists = FileManager.default.fileExists(atPath: self.path, isDirectory: &isDirectory)
        return exists && isDirectory.boolValue
    }
    
    func makeSureDirExist() -> Result<URL,Error> {
        if self.isDirExist {
            return .success(self)
        } else {
            return self.mkdir()
        }
    }
    
    static var tempDir : URL {
//        if #available(macOS 10.12, *) {
//            return FileManager.default.temporaryDirectory
//        } else {
            return URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        //}
    }
    
    static func tmp(_ type: TmpDir, prefix: String = "") -> Result<URL, Error> {
        switch type {
        case .userRoot:
            return .success(URL.tempDir.appendingPathComponent(prefix, isDirectory: true))
        case .userUnique:
            return .success(URL.tempDir.appendingPathComponent(prefix).appendingPathComponent(UUID().uuidString, isDirectory: true))
        case .systemRoot:
            return .success(URL(fileURLWithPath: "/tmp/", isDirectory: true).appendingPathComponent(prefix, isDirectory: true))
        case .systemUnique:
            return .success(URL(fileURLWithPath: "/tmp/", isDirectory: true).appendingPathComponent(prefix).appendingPathComponent(UUID().uuidString, isDirectory: true))
        }
    }
    
    static func randomTempDirectory() -> Result<URL,Error> {
        let url = URL.tempDir.appendingPathComponent(UUID().uuidString)
        return url.makeSureDirExist()
    }
    
    func mkdir() -> Result<URL,Error> {
        let fileManager = FileManager.default
        do {
            try fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } catch {
            return .failure(error)
        }
        
        return .success(self)
    }
        
    var exists   : Bool  { FileManager.default.fileExists(atPath: self.path) }
    
    func rm() -> Result<(),Error> {
        do {
            try FileManager.default.removeItem(atPath: self.path)
        } catch {
            if let err = (error as NSError).userInfo["NSUnderlyingError"] as? NSError {
                if err.domain == "NSPOSIXErrorDomain" || err.code == 4 { // if file does'nt exist, do not treat it like error
                    return .success(())
                }
            }
            
            return .failure(error)
        }
        
        return .success(())
    }
    
    var fileHande : R<FileHandle> {
        do {
            return .success(try FileHandle(forUpdating: self))
        } catch {
            return .failure(error)
        }
    }
    
    func write(content: String) -> Result<URL, Error> {
        do {
            try content.write(to: self, atomically: true, encoding: .utf8)
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    func write(data: Data) -> Result<URL, Error> {
        do {
            try data.write(to: self)
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    @available(macOS 10.15.4, *)
    func append(content: String) -> R<()> {
        print(content)
        return combine(content.asData(), self.fileHande | { $0.seekToEnd_r() })
            | { data, handler in handler.write_r(data) }
            | { $0.close_r() }
            | { _ in () }
    }
    
    var readToString : R<String> {
        do {
            let s = try String(contentsOf: self)
            return .success(s)
        } catch {
            return .failure(error)
        }
    }
    
    var readToData : R<Data> {
        do {
            let data = try Data(contentsOf: self)
            return .success(data)
        } catch {
            return .failure(error)
        }
    }
    
    
    var isDirectory: Bool {
        // WTF!?!?!??!
        // Is there exist some way to do this CORRECTLY???????
        // WTF!?!?!??!
        
        // the following code woks not always for some reason:
        ///IMPORTANT: this code return false even if file or directory does not exist(!!!)
        // return hasDirectoryPath
        
        // the following code woks not always for some reason:
        //return (try? resourceValues(forKeys: [.isDirectoryKey]))?.isDirectory == true
        
        //Code duplicates of path.FS.info.isDirectory
        var check: ObjCBool = false
        
        if FileManager.default.fileExists(atPath: self.path, isDirectory: &check) {
            return check.boolValue
        } else {
            return false
        }
    }
    
    var isEmptyDirectory: Bool {
        return !isDirContainsFiles()
    }
    
    var isFileExists: Bool {
        if self.isDirectory { return false }
        return (try? self.checkResourceIsReachable()) ?? false
    }
    
    func isDirContainsFiles() -> Bool {
        do {
            let contents = try FileManager.default.contentsOfDirectory(atPath: self.path)
            
            if contents.count > 0 { return true }
        } catch { }
        
        return false
    }
    
    var files : R<[String]> {
        do {
            let files = try FileManager.default.contentsOfDirectory(atPath: self.path)
            return .success(files)
        } catch {
            return .failure(error)
        }
    }
    
    func getFiles() -> [URL] {
        var urls : [URL] = []
        
        if self.isDirExist {
            let enumerator:FileManager.DirectoryEnumerator? = FileManager.default.enumerator(at: self, includingPropertiesForKeys: nil, options: [], errorHandler: nil)
            
            while let url = enumerator?.nextObject() as? URL {
                if url.lastPathComponent == ".DS_Store" {
                    continue
                }
                urls.append(url)
            }
        }
        
        return urls
    }
    
    func files(ofType: Set<String>) -> R<[URL]> {
        var files = [URL]()
        if let enumerator = FileManager.default.enumerator(at: self, includingPropertiesForKeys: [.isRegularFileKey], options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
            for case let fileURL as URL in enumerator {
                do {
                    let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey])
                    if fileAttributes.isRegularFile ?? false {
                        if ofType.contains(fileURL.pathExtension) {
                            files.append(fileURL)
                        }
                    }
                } catch {
                    return .failure(error)
                }
            }
        }
        return .success(files)
    }
    
    func getSubDirectories() -> [URL] {
        guard isDirectory else { return [] }
        return (try? FileManager.default.contentsOfDirectory(at: self, includingPropertiesForKeys: nil, options: [.skipsHiddenFiles]).filter(\.isDirectory)) ?? []
    }
}


public extension URL {
    static var userHome : URL   {
        URL(fileURLWithPath: userHomePath, isDirectory: true)
    }
    
    static var userHomePath : String   {
        let pw = getpwuid(getuid())
        if let home = pw?.pointee.pw_dir {
            return FileManager.default.string(withFileSystemRepresentation: home, length: Int(strlen(home)))
        }
        
        fatalError()
    }
}

extension URL : Comparable {
    public static func < (lhs: URL, rhs: URL) -> Bool {
        lhs.path < rhs.path
    }
}

public extension URL {
    static func generageEmailUrl(email: String, subject: String = "", body: String = "") -> URL {
        if let encodedParams = "subject=\(subject)&body=\(body)"
            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            return URL(string: "mailto:\(email)?\(encodedParams)")!
        }
        
        return  URL(string: "mailto:\(email)")!
    }
}

public extension URL {
    var lastPathComponentOSLocalized: String {
        FileManager.default.displayName(atPath: self.path)
    }
}

public extension String {
    var lastPathComponentOSLocalized: String {
        FileManager.default.displayName(atPath: self)
    }
}

public extension URL {
    var urlHost: String {
        self.absoluteString.extract(regExp: "^(([^:/?#]+):)?^(([^:/?#]+):)?(//([^/?#]*))?").maybeSuccess!
    }
}
