
import Foundation
import CoreFoundation
#if os(macOS)
import AppKit

public struct WebBrowser : Identifiable {
    public var id: URL { self.appURL }
    
    let pool = FSPoolID(id: "WebBrowser").pool
    public let appURL : URL
    
    init(url: URL) { self.appURL = url }
    
    public var displayName : String { appURL.deletingPathExtension().lastPathComponent }
    
    @discardableResult
    public func open(url: URL) -> Flow.Future<Void> {
        pool.promise { promise in
            NSWorkspace.shared.open([url], withApplicationAt: appURL, configuration: NSWorkspace.OpenConfiguration()) { app, error in
                if let error = error {
                    promise.complete(.failure(error))
                }
                if let _ = app {
                    promise.complete(.success(()))
                }
            }
        }
    }
    
    @discardableResult
    public func open(url: String) -> Flow.Future<Void> {
        guard let realURL = URL(string: url) else { return .failed(WTF("can't convert to URL: \(url)")) }
        return open(url: realURL)
    }
}

@available(macOS 12.0, *)
extension WebBrowser {
    public static var main : WebBrowser { WebBrowser.list.first ?? WebBrowser(url: URL(fileURLWithPath: "/Applications/Safari.app")) }
    
    public static var appsToFilter : Set<String> { Set(["Downie", "Pulltube", "Movist Pro", "Folx"]) }
    
    public static var list : [WebBrowser] {
        guard let urlScheme = URL(string: "https://") else { return [] }

        return NSWorkspace.shared.urlsForApplications(toOpen: urlScheme).map { WebBrowser(url: $0) }
    }
    
    public static var filteredList : [WebBrowser] { list.filter { !appsToFilter.contains($0.displayName) } }
    
    public var icon : NSImage {
        NSWorkspace.shared.icon(forFile: appURL.path)
    }
}
#endif
