public protocol ThreadSafeAccessClass : AnyObject {
    var lock : UnfairLock { get }
}

//extension ThreadSafeAccess {
//    func findFirst<T>(in: KeyPath<Self,Array<T>>, block: (T)->Bool) -> T? {
//        self[keyPath: `in`].first(where: block)
//    }
//}

public extension ThreadSafeAccessClass {
    func read<T>(from keyPath: KeyPath<Self,T>) -> T {
        lock.locked {
            self[keyPath: keyPath]
        }
    }
    
    func read<T,T2>(from keyPath: KeyPath<Self,T>, block: (T)->(T2)) -> T2 {
        block(lock.locked { self[keyPath: keyPath]})
    }
    
    func read<T1,T2>(from keyPath: KeyPath<Self,T1>, at path: KeyPath<T1,T2>) -> T2 {
        lock.locked { self[keyPath: keyPath][keyPath: path] }
    }
    
    func cache<Key,Value>(keyPath: ReferenceWritableKeyPath<Self,R<Value>.LazyDic<Key>>, key: Key) -> R<Value> {
        let lazyDic = lock.locked { self[keyPath: keyPath] }
        if let value = lazyDic.dic[key] {
            return .success(value)
        }
        
        return lazyDic.getter(key).onSuccess { value in
            lock.locked {
                self[keyPath: keyPath].dic[key] = value
            }
        }
    }
    
    func cache<Key,Value>(keyPath: ReferenceWritableKeyPath<Self,LazyDic<Key,Value>>, key: Key) -> Value {
        let lazyDic = lock.locked { self[keyPath: keyPath] }
        if let value = lazyDic.dic[key] {
            return value
        }
                
        let value = lazyDic.getter(key)
        lock.locked {
            self[keyPath: keyPath].dic[key] = value
        }
        
        return value
    }
    
    func map<T>(keyPath: ReferenceWritableKeyPath<Self,T>, block: (T)->(T)) {
        let value = lock.locked { self[keyPath: keyPath] }
        let result = block(value)
        lock.locked {
            self[keyPath: keyPath] = result
        }
    }
    
    func write<T,T2>(keyPath: ReferenceWritableKeyPath<Self,T>, block: (inout T)->(T2)) -> T2 {
        lock.locked {
            block(&self[keyPath: keyPath])
        }
    }
}
