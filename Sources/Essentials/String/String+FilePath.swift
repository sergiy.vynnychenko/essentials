import Foundation

public extension String {
    var skippingLastSlash : String {
        if self.hasSuffix("/") {
            return String(self.dropLast())
        }
        
        return self
    }
    
    func removeFileExtension() -> String {
        for i in stride(from: self.count, to: 0, by: -1) {
            if i > 0 {
                if self[i] == "." && self[i-1] != "/" {
                    return substring(to: i)
                }
            }
        }
        
        return self
    }
    
    
    var lastPathComonent : String {
        String(self.split(separator: "/").last ?? "" )
    }
    
    var getExceptLastPathComponent: String {
        self.split(separator: "/").dropLast().joined(separator: "/")
    }
}

extension Substring {
    public func deletingLastPathComponent() -> Substring? {
        var counter = self.last == "/" ? 2 : 1
        
        while self[self.count - counter] != "/" {
            if self.count - counter == 1 {
                return nil
            }
            
            counter += 1
        }
        
        let b = self.dropLast(counter)
        
        return b
    }
}

extension String {
    public func deletingLastPathComponent() -> Substring? {
        self.getSubstring(location: 0, length: self.count)?
            .deletingLastPathComponent()
    }
}
