import Foundation

public extension String {
    func asURLdir() -> URL {
        URL(fileURLWithPath: self, isDirectory: true )
    }
    
    func asURL(isDirectory: Bool? = nil) -> URL {
        return isDirectory == nil ? URL(fileURLWithPath: self) : URL(fileURLWithPath: self, isDirectory: isDirectory! )
    }
    
    func asBrowserUrl() -> URL? {
        return URL(string: self)
    }
}
