public extension UnsafeMutablePointer where Pointee == CChar {
    func asString() -> String {
        return String(cString: self )
    }
}
