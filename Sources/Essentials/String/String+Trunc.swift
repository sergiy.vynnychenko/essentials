import Foundation

public extension String {
    func truncEnd(length: Int, trailing: String = "…") -> String {
        if (self.count <= length) { return self }
        
        let truncated = self
            .dropLast( self.count - length)
        
        return truncated + trailing
    }
    
    func truncStart(length: Int, trailing: String = "…") -> String {
        if (self.count <= length) { return self }
        
        let truncated = self
            .dropFirst( self.count - length)
        
        return trailing + truncated
    }
    
    func truncCenter(length: Int, trailing: String = "…") -> String {
        if (self.count <= length) { return self }
        
        let first = self.substring(to: length/2)
        let last = self.substring(from: self.count - length/2)
        
        return first + trailing + last
    }
}
