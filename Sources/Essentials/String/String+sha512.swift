#if os(macOS)
import Foundation
import CommonCrypto

public extension String {
    
    var sha512: Data? {
        let length = Int(CC_SHA512_DIGEST_LENGTH)
        guard let messageData = self.data(using:.utf8) else { return nil }
        var digestData = Data(count: length)

        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_SHA512(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }

}
#endif
