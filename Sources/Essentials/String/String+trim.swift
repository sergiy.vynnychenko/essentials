import Foundation

public let whitespaceChars: [Character] = Array(NSCharacterSet.whitespacesAndNewlines)

public extension String {
    func trim(_ char: Character) -> String {
        self.trimStart(char).trimEnd(char)
    }
    
    func trim(_ symbols: [Character] = whitespaceChars ) -> String {
        self.trimStart(symbols).trimEnd(symbols)
    }
    
    func trimStart(_ char: Character) -> String {
        return trimStart([char])
    }
    
    func trimStart(_ symbols: [Character] = whitespaceChars) -> String {
        var startIndex = 0
        
        for char in self {
            if symbols.contains(char) {
                startIndex += 1
            }
            else {
                break
            }
        }
        
        if startIndex == 0 {
            return self
        }
        
        return String( self.substring(from: startIndex) )
    }
    
    func trimEnd(_ char: Character) -> String {
        return trimEnd([char])
    }
    
    func trimEnd(_ symbols: [Character] = whitespaceChars) -> String {
        var endIndex = self.count - 1
        
        if endIndex == -1 {
            return self
        }
        
        for i in (0...endIndex).reversed() {
            if symbols.contains( self[i] ) {
                endIndex -= 1
            }
            else {
                break
            }
        }
        
        if endIndex == self.count {
            return self
        }
        
        return String( self.substring(to: endIndex + 1) )
    }
}
