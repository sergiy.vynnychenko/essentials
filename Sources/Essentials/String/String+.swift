import Foundation
import SwiftUI

public extension NSRegularExpression {
    func matches (_ inStr: String) -> [NSTextCheckingResult] {
        let range = NSRange(inStr.startIndex..., in: inStr)
        
        return self.matches(in: inStr, options: [], range: range)
    }
}

public extension String {
    func arrayOfMatchesFor(regex pattern: String) -> [String] {
        guard let regex = try? NSRegularExpression(pattern: pattern, options: [])
        else { return [] }
        
        return regex.matches(self)
            .map { Range($0.range, in: self) }
            .compactMap{ $0 }
            .map{ String( self[$0] ) }
    }
    
    func isMatchToRegex(regex pattern: String) -> Bool {
        if let firstMatch = self.arrayOfMatchesFor(regex: pattern).first {
            return firstMatch == self
        }
        
        return false
    }
    
    func split(byCharsIn str: String) -> [String] {
        self.components(separatedBy: CharacterSet(charactersIn: str))
    }
    
    /// This is NOT efficient solution
    func split<T>(bySeparators separators: [T]) -> [String] where T : StringProtocol {
        var result = [self]
        for separator in separators {
            result = result
                .map { $0.components(separatedBy: separator)}
                .flatMap { $0 }
        }
        
        return result
    }
}

// Subscript
public extension String {
    subscript (i: Int) -> String {
        get {
            return self[i ..< i + 1]
        }
        set (value) {
            let range = index(startIndex, offsetBy: i) ..< index(startIndex, offsetBy: i+1)
            self.replaceSubrange(range, with : value)
        }
    }
    
    func substring(from: Int) -> String {
        return self[min(from, count) ..< count]
    }
    
    func substring(to: Int) -> String {
        return self[0 ..< max(0, to)]
    }
    
    subscript (r: Range<Int>) -> String {
        get {
            let range = Range(uncheckedBounds: (lower: max(0, min(count, r.lowerBound)),
                                                upper: min(count, max(0, r.upperBound))))
            let start = index(startIndex, offsetBy: range.lowerBound)
            let end = index(start, offsetBy: range.upperBound - range.lowerBound)
            return String(self[start ..< end])
        }
        set (value) {
            let range = Range(uncheckedBounds: (lower: max(0, min(count, r.lowerBound)),
                                                upper: min(count, max(0, r.upperBound))))
            let start = index(startIndex, offsetBy: range.lowerBound)
            let end = index(start, offsetBy: range.upperBound - range.lowerBound)
            
            self.replaceSubrange(start ..< end, with : value)
        }
    }
    
    subscript (nsrange: NSRange) -> String {
        get {
            if let range = Range(nsrange, in: self) {
                return String(self[range])
            } else {
                return ""
            }
        }
        
        set {
            if let range = Range(nsrange, in: self) {
                self.replaceSubrange(range, with: newValue)
            }
        }
    }
    
    func indexInt(of char: Character) -> Int? {
        return firstIndex(of: char)?.utf16Offset(in: self)
    }
    
    func indexInt(of str: any StringProtocol) -> Int? {
        return index(of: str)?.utf16Offset(in: self)
    }
    
    ///  Created by DragonCherry on 5/11/17.
    /// Inner comparison utility to handle same versions with different length. (Ex: "1.0.0" & "1.0")
    private func compare(toVersion targetVersion: String) -> ComparisonResult {
        
        let versionDelimiter = "."
        var result: ComparisonResult = .orderedSame
        var versionComponents = components(separatedBy: versionDelimiter)
        var targetComponents = targetVersion.components(separatedBy: versionDelimiter)
        let spareCount = versionComponents.count - targetComponents.count
        
        if spareCount == 0 {
            result = compare(targetVersion, options: .numeric)
        } else {
            let spareZeros = repeatElement("0", count: abs(spareCount))
            if spareCount > 0 {
                targetComponents.append(contentsOf: spareZeros)
            } else {
                versionComponents.append(contentsOf: spareZeros)
            }
            result = versionComponents.joined(separator: versionDelimiter)
                .compare(targetComponents.joined(separator: versionDelimiter), options: .numeric)
        }
        return result
    }
    
    func isVersion(equalTo targetVersion: String)              -> Bool { return compare(toVersion: targetVersion) == .orderedSame }
    func isVersion(greaterThan targetVersion: String)          -> Bool { return compare(toVersion: targetVersion) == .orderedDescending }
    func isVersion(greaterThanOrEqualTo targetVersion: String) -> Bool { return compare(toVersion: targetVersion) != .orderedAscending }
    func isVersion(lessThan targetVersion: String)             -> Bool { return compare(toVersion: targetVersion) == .orderedAscending }
    func isVersion(lessThanOrEqualTo targetVersion: String)    -> Bool { return compare(toVersion: targetVersion) != .orderedDescending }

#if os(macOS)
    func isEmail() -> Bool {
        let firstpart = "[A-Z0-9a-z]([A-Z0-9a-z._%+-]{0,30}[A-Z0-9a-z])?"
        let serverpart = "([A-Z0-9a-z]([A-Z0-9a-z-]{0,30}[A-Z0-9a-z])?\\.){1,5}"
        let emailRegex = firstpart + "@" + serverpart + "[A-Za-z]{2,8}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailPredicate.evaluate(with: self)
    }
#endif
    
    func withReplacing(from: String, to: String) -> String {
        return replacingOccurrences(of: from, with: to, options: NSString.CompareOptions.literal, range: nil)
    }
    
    @available(*, deprecated, message: "use extract(regExp:) instead")
    func extractRegExp(pattern: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: pattern) else { return nil }
        let range = NSRange(location: 0, length: utf16.count)
        
        if let result = regex.firstMatch(in: self, options: [], range: range) {
            return self[result.range]
        }
        
        return nil
    }
    
    func extract(regExp pattern: String) -> Result<String, Error> {
        do {
            let regex = try NSRegularExpression(pattern: pattern)
            let range = NSRange(location: 0, length: utf16.count)
            
            if let result = regex.firstMatch(in: self, options: [], range: range) {
                return .success(self[result.range])
            } else {
                return .failure(WTF("can't find first match for RegExt: \(pattern)"))
            }
            
        } catch {
            return .failure(error)
        }
    }
}

public extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func replace(of: String, to: String) -> String {
        return self.replacingOccurrences(of: of, with: to, options: .regularExpression, range: nil)
    }
    
    @available(*, deprecated, message: "use replacing(allOf:, to) instead")
    func replace(allOf: [String], to: String) -> String {
        return allOf.reduce(self) { acum, repl in acum.replacingOccurrences(of: repl, with: to) }
    }
    
    func replacing(allOf: [String], to: String) -> String {
        return allOf.reduce(self) { acum, repl in acum.replacingOccurrences(of: repl, with: to) }
    }
}

public extension String {
    static func * (str: String, repeatTimes: Int) -> String {
        return String(repeating: str, count: repeatTimes)
    }
    
    static func * (repeatTimes: Int, str: String) -> String {
        return String(repeating: str, count: repeatTimes)
    }
    
    // Making string with exact length
    func force(length: Int) -> String {
        let spacesCount = length - self.count
        
        guard spacesCount >= 0 else { return self }
        
        return self + " " * spacesCount
    }
    
    func dropFirstCustom(_ dropFirst : Int) -> String {
        if self.count < dropFirst {
            return "\n"
        }
        
        return self
            .dropFirst(dropFirst)
            .asStr()
    }
}

public extension String {
    func stringBefore(_ delimiter: Character) -> String {
        if let index = firstIndex(of: delimiter) {
            return String(prefix(upTo: index))
        } else {
            return ""
        }
    }
    
    func stringAfter(_ delimiter: Character) -> String {
        if let index = firstIndex(of: delimiter) {
            return String(suffix(from: index).dropFirst())
        } else {
            return ""
        }
    }
}

public extension String {
    func asData() -> Result<Data, Error> {
        if let data = self.data(using: .utf8) {
            return .success(data)
        } else {
            return .failure(WTF("can't convert string to data: \(self)"))
        }
    }
// Commented 2021.12.21. Delete me in 1 week period
//    var asURL : R<URL> {
//        if let url = URL(string: self) {
//
//            return .success(url)
//        }
//        return .failure(WTF("Failed String.asURL: \(self)"))
//    }
}

//////////////////////////////////
/// Prefix & Suffix & Postfix
/// prefix, sufix -> ends(with), starts(with)
/////////////////////////////////

public extension StringProtocol {
    func hasSuffix(_ suffixes:[String]) -> Bool {
        for s in suffixes {
            if self.hasSuffix(s) {
                return true
            }
        }
        
        return false
    }
    
    func hasPrefix(_ prefixes:[String]) -> Bool {
        for s in prefixes {
            if self.hasPrefix(s) {
                return true
            }
        }
        
        return false
    }
}

public extension StringProtocol {
    func ends(with suffix: String) -> Bool {
        self.hasSuffix(suffix)
    }
    
    func starts(with prefix: String) -> Bool {
        self.hasPrefix(prefix)
    }
    
    func startsWith(oneOf strs: [String]) -> Bool {
        hasPrefix(strs)
    }
    
    func endsWith(oneOf strs: [String]) -> Bool {
        hasSuffix(strs)
    }
}

public extension StringProtocol {
    func equalTo(oneOf strs: [String]) -> Bool {
        for itm in strs {
            if self == itm {
                return true
            }
        }
        
        return false
    }
    
    func contains(oneOf strs: [String]) -> Bool {
        for str in strs {
            if self.contains(str) {
                return true
            }
        }
        
        return false
    }
}

public extension String.SubSequence {
    func removePostfix(_ postfix: String) -> String {
        return "\(self)".removePostfix(postfix)
    }
}

public extension String {
    func removePostfix(_ postfix: String) -> String {
        if self.count < postfix.count {
            return "\(self)"
        }
        
        let postfixOfStr = self.substring(from: self.count - postfix.count)
        if postfixOfStr == postfix {
            return self.substring(to: self.count - postfix.count)
        }
        
        return "\(self)"
    }
}

//////////////////////////////////
/// String[safe Range]
/// String[unsafe Range]
/////////////////////////////////

public extension StringProtocol {
    /// Use this if you want to get OutOfBounds exception
    subscript(unsafe bounds: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: bounds.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: bounds.count)]
    }
    
    /// Use this if you want to get OutOfBounds exception
    subscript(unsafe bounds: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: bounds.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: bounds.count)]
    }
}

public extension String {
    /// Use this if you want to get result with any incorrect input
    subscript(safe bounds: CountableClosedRange<Int>) -> SubSequence {
        let lowerBound = max(0, Int(bounds.lowerBound) )
        
        guard lowerBound < self.count else { return "" }
        
        let upperBound = min(Int(bounds.upperBound), self.count-1)
        
        guard upperBound >= 0 else { return "" }
        
        let minIdx = index(startIndex, offsetBy: lowerBound )
        let maxIdx = index(minIdx, offsetBy: upperBound-lowerBound )
        
        return self[minIdx...maxIdx]
    }
    
    /// Use this if you want to get result with any incorrect input
    subscript(safe bounds: CountableRange<Int>) -> SubSequence {
        let lowerBound = max(0, bounds.lowerBound)
        
        guard lowerBound < self.count else { return "" }
        
        let upperBound = min(bounds.upperBound, self.count)
        
        guard upperBound >= 0 else { return "" }
        
        let minIdx = index(startIndex, offsetBy: lowerBound )
        let maxIdx = index(minIdx, offsetBy: upperBound-lowerBound )
        
        return self[minIdx..<maxIdx]
    }
}

//////////////////////////////////
/// String.getSubstring()
/////////////////////////////////
public extension String {
    func getSubstring(location: Int, length: Int) -> Substring? {
        guard location >= 0 || length >= 0,
              location <= self.count
        else { return nil }
        
        guard location + length <= self.count else { return self.dropFirst(location) }
        
        return self.dropFirst(location).dropLast( self.count - location - length)
    }
    
    func getSubstring(range: NSRange) -> Substring? {
        return getSubstring(location: range.location, length: range.length)
    }
}

//////////////////////////////////
/// String.regex()
/////////////////////////////////
public extension String {
    func regex(pattern: String) -> [Substring] {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options(rawValue: 0))
            let all = NSRange(location: 0, length: self.count)
            
            var matches : [Substring] = []
            
            regex.enumerateMatches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all)
                { (result : NSTextCheckingResult?, _, _) in
                    if let r = result,
                        let subStr = self.getSubstring(range: r.range)
                    {
                        matches.append(subStr)
                    }
                }
            return matches
        } catch {
            return []
        }
    }
}

/////////////////////////
/// ACCESS TO CHAR BY INDEX
////////////////////////
extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}

public extension StringProtocol {
    subscript(i idx: Int) -> Character? {
        if idx >= self.count { return nil }
        
        return self[self.index(self.startIndex, offsetBy: idx)]
    }
    
    func asCharArr() -> [Character] {
        return Array(self)
    }
}

public extension Substring {
    subscript(i idx: Int) -> Character? {
        if idx >= self.count { return nil }
        return self.base[index(startIndex, offsetBy: idx)]
    }
}

//////////////////////////////////
///Index(of )
/////////////////////////////////
public extension StringProtocol {
    func index<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.lowerBound
    }
    
    func endIndex<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> Index? {
        range(of: string, options: options)?.upperBound
    }
    
    func indices<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Index] {
        ranges(of: string, options: options).map(\.lowerBound)
    }
    
    func ranges<S: StringProtocol>(of string: S, options: String.CompareOptions = []) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var startIndex = self.startIndex
        while startIndex < endIndex,
            let range = self[startIndex...]
                .range(of: string, options: options) {
                result.append(range)
                startIndex = range.lowerBound < range.upperBound ? range.upperBound :
                    index(range.lowerBound, offsetBy: 1, limitedBy: endIndex) ?? endIndex
        }
        return result
    }
}

public extension Character {
    func asString() -> String {
        "\(self)"
    }
}

//////////////////////////////////
///SwiftUI
/////////////////////////////////

extension String {
    @available(macOS 11.0, *)
    @available(iOS 15.0, *)
    public var sfImg: Image {
        Image(systemName: self)
    }
}

//////////////////////////
///DEPRECATED
/////////////////////////
public extension String.SubSequence {
    @available(*, deprecated, message: "use .asStr() func instead")
    func asString() -> String {
        return String(self)
    }
}

public extension CustomStringConvertible {
    func asStr() -> String {
        self.description
    }
}

public extension StringProtocol {
    func substring(from: String, toFirst: String) -> String {
        guard let start = self.index(of: from)
        else { return "" }
        
        let subStr = self[start..<self.endIndex]
        
        guard let end = subStr.range(of: toFirst)?.lowerBound
        else { return "" }
        
        return "\(self[start..<end])"
    }
    
    func substring(between str1: String, and str2: String) -> String {
        guard let start = self.range(of: str1)?.upperBound
        else { return "" }
        
        let subStr = self[start..<self.endIndex]
        
        guard let end = subStr.range(of: str2)?.lowerBound
        else { return "" }
        
        return "\(self[start..<end])"
    }
}

public extension String {
    func first(_ num: Int) -> String {
        self.substring(to: num)
    }
}
