import Foundation

public extension CaseIterable {
    /// After last will be first
    func nextCircular() -> Self.AllCases.Element where Self: RawRepresentable, Self.RawValue: Comparable {
        let nextRaw = Self.allCases.map{ $0.rawValue }.circleIterationNext(relatedTo: self.rawValue)!
        
        return Self(rawValue: nextRaw)!
    }
    
    /// After first will be last
    func prevCircular() -> Self.AllCases.Element where Self: RawRepresentable, Self.RawValue: Comparable  {
        let nextRaw = Self.allCases.map{ $0.rawValue }.circleIterationPrev(relatedTo: self.rawValue)!
        
        return Self(rawValue: nextRaw)!
    }
    
    /// In case of last - will return nil
    func next() -> Self.AllCases.Element? where Self: RawRepresentable, Self.RawValue: Comparable {
        let last = Self.allCases.map{ $0.rawValue }.last
        
        if self.rawValue == last {
            return nil
        }
        
        return nextCircular()
    }
    
    /// In case of first - will return nil
    func prev() -> Self.AllCases.Element? where Self: RawRepresentable, Self.RawValue: Comparable  {
        let first = Self.allCases.map{ $0.rawValue }.first
        
        if self.rawValue == first {
            return nil
        }
        
        return prevCircular()
    }
}
