#if os(macOS)
import Foundation

public extension XR {
    
    struct Shell {
        public let cmd : String
        public let workDir : URL
        
        public init(cmd: String, workDir: URL = URL.userHome) {
            self.cmd = cmd
            self.workDir = workDir
        }
        
        public func run(args: [String]?, waitUntilExit: Bool = true) -> Process {
            let standardOutputPipe = Pipe()
            let standardErrorPipe = Pipe()
            let task = Process()
            if #available(macOS 10.13, *) {
                task.currentDirectoryURL = workDir
            } else {
                // Fallback on earlier versions
            }
            task.standardOutput = standardOutputPipe
            task.standardError  = standardErrorPipe
            task.launchPath = cmd
            task.arguments = args
            task.launch()
            if waitUntilExit {
                task.waitUntilExit()
            }
            
            return task
        }
        
        public func run3(args: [String]?) -> R<String> {
            var result = String()
            var errors = [String]()
            let standardOutputPipe = Pipe()
            let standardErrorPipe = Pipe()
            let task = Process()
            if #available(macOS 10.13, *) {
                task.currentDirectoryURL = workDir
            } else {
                // Fallback on earlier versions
            }
            task.standardOutput = standardOutputPipe
            task.standardError  = standardErrorPipe
            task.launchPath = cmd
            task.arguments = args
            
            
            standardOutputPipe.fileHandleForReading.readabilityHandler = { fh in
                let data = fh.availableData
                if data.isEmpty { // EOF on the pipe
                    standardOutputPipe.fileHandleForReading.readabilityHandler = nil
                } else {
                    if let str = String(data: data, encoding: .utf8) {
                        result.append(str)
                    }
                }
            }
            
            standardErrorPipe.fileHandleForReading.readabilityHandler = { fh in
                let data = fh.availableData
                if data.isEmpty { // EOF on the pipe
                    standardErrorPipe.fileHandleForReading.readabilityHandler = nil
                } else {
                    errors.append(String(data: data, encoding: .utf8)!)
                }
            }
            
            task.launch()
            task.waitUntilExit()
            
            if let lastError = errors.last {
                return .wtf(lastError)
            }
            
            return .success(result)
        }

    }
}

extension XR.Shell {
    public func run2(args: [String]?) -> R<String> {
        let standardOutputPipe = Pipe()
        let standardErrorPipe = Pipe()
        let task = Process()
        if #available(macOS 10.13, *) {
            task.currentDirectoryURL = workDir
        } else {
            // Fallback on earlier versions
        }
        task.standardOutput = standardOutputPipe
        task.standardError  = standardErrorPipe
        task.launchPath = cmd
        task.arguments = args
        
        task.launch()
        
        var resultStr = ""
        var errorStr = ""
        
        while task.isRunning {
            if let outputPipe = task.standardOutput as? Pipe {
                let data = outputPipe.fileHandleForReading.availableData
                
                if !data.isEmpty {
                    if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
                        resultStr += line
                    }
                }
            }
            
            if let errorPipe = task.standardError as? Pipe {
                let data = errorPipe.fileHandleForReading.availableData
                
                if !data.isEmpty {
                    if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
                        errorStr += line
                    }
                }
            }
        }
        
        task.waitUntilExit()
        
        if errorStr.count > 0 {
            return .failure(WTF(errorStr))
        }
        
        if resultStr.count > 0 {
            return .success(resultStr)
        }
        
        return .success("")
    }
}



public extension Process {
//    @available(macOS 12, *)
//    var outputAsString2: R<String>  {
//        guard let pipe = standardOutput as? Pipe else { return .failure(WTF("OutputPipe is nil")) }
//
//        let semaphore = DispatchSemaphore(value: 0)
//
//        let res = Task { () -> String in
//            var result = ""
//
//            for try await line in pipe.bytes.lines {
//                result += "\(line)\n"
//            }
//
//            semaphore.signal()
//
//            return result
//        }
//
//
//    }
    
    
    
//    @available(macOS 10.15.4, *)
    var outputAsString : R<String> {
        let outputPipe = standardOutput as? Pipe
        let errorPipe = standardError as? Pipe
        
        var resultStr = ""
        var errorStr = ""
        
//        if self.ou
        // TODO: It's ugly, pls refactor
        if self.isRunning {
            while self.isRunning {
                if let outputPipe = outputPipe {
                    let data = outputPipe.fileHandleForReading.availableData
                    
                    if !data.isEmpty {
                        if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
//                            print(line)


                            resultStr += line
                        }
                    }
                }
                
                if let errorPipe = errorPipe {
                    let data = errorPipe.fileHandleForReading.availableData
                    
                    if !data.isEmpty {
                        if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
//                            print(line)
                            errorStr += line
                        }
                    }
                }
            }
        } else {
            if let outputPipe = outputPipe {
                let data = outputPipe.fileHandleForReading.availableData
                
                if !data.isEmpty {
                    if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
//                        print(line)
                        resultStr += line
                    }
                }
            }
            
            if let errorPipe = errorPipe {
                let data = errorPipe.fileHandleForReading.availableData
                
                if !data.isEmpty {
                    if let line = String(data: data, encoding: .utf8)?.trimmingCharacters(in: .whitespaces) {
//                        print(line)
                        errorStr += line
                    }
                }
            }
        }
        
        if errorStr.count > 0 {
            return .failure(WTF(errorStr))
        }
        
        if resultStr.count > 0 {
            return .success(resultStr)
        }
        
        return .success(resultStr)
        
    }
}

//@available(macOS 10.15, *)
//public extension Pipe {
//    struct AsyncBytes: AsyncSequence {
//        public typealias Element = UInt8
//
//        let pipe: Pipe
//
//        public func makeAsyncIterator() -> AsyncStream<Element>.Iterator {
//            AsyncStream { continuation in
//                pipe.fileHandleForReading.readabilityHandler = { handle in
//                    for byte in handle.availableData {
//                        continuation.yield(byte)
//                    }
//                }
//
//                continuation.onTermination = { _ in
//                    pipe.fileHandleForReading.readabilityHandler = nil
//                }
//            }.makeAsyncIterator()
//        }
//    }
//
//    var bytes: AsyncBytes { AsyncBytes(pipe: self) }
//}




#endif

//@available(macOS 10.15, *)
//class UnsafeTask<T> {
//    let semaphore = DispatchSemaphore(value: 0)
//    private var result: T?
//    init(block: @escaping () async -> T) {
//        Task {
//            result = await block()
//            semaphore.signal()
//        }
//    }
//
//    func get() -> T {
//        if let result = result { return result }
//        semaphore.wait()
//        return result!
//    }
//}
