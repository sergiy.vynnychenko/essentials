
import Foundation
import CoreFoundation
#if os(macOS)
import AppKit

@available(macOS 12.0, *)
public struct MacApp : Identifiable, Hashable {
    public var id: URL { self.appURL }
    
    var pool : FSPool { FSPoolID(id: "MacApp").pool }
    public let appURL : URL
    public let fileURL: URL
    
    init(url: URL, fileURL: URL) { self.appURL = url; self.fileURL = fileURL }
    
    public var displayName : String { appURL.deletingPathExtension().lastPathComponent }
    
    @discardableResult
    public func open() -> Flow.Future<Void> {
        pool.promise { promise in
            
            if appURL == MacApp.finderURL {
                NSWorkspace.shared.activateFileViewerSelecting([fileURL])
                promise.complete(.success(()))
            } else {
                
                NSWorkspace.shared.open([fileURL], withApplicationAt: appURL, configuration: NSWorkspace.OpenConfiguration()) { app, error in
                    if let error = error {
                        promise.complete(.failure(error))
                    }
                    if let _ = app {
                        promise.complete(.success(()))
                    }
                }
            }
        }
        .failing(into: pool)
    }
}

@available(macOS 12.0, *)
extension MacApp {
    public static var finderURL : URL { URL(fileURLWithPath: "/System/Library/CoreServices/Finder.app") }
//    public static var finder : MacApp { MacA }
    
    public static func list(fileURL: URL, withFinder: Bool = false) -> [MacApp] {
        let list = NSWorkspace.shared.urlsForApplications(toOpen: fileURL).map { MacApp(url: $0, fileURL: fileURL) }
        if withFinder {
            let finderApp = MacApp(url: MacApp.finderURL, fileURL: fileURL)
            return [finderApp].appending(contentsOf: list)
        } else {
            return list
        }
    }
    
    public var icon : NSImage {
        NSWorkspace.shared.icon(forFile: appURL.path)
    }
}
#endif
