public struct LazyDic<Key:Hashable,Value> {
    public var dic : Dictionary<Key,Value>
    public var getter : (Key) -> Value
    
    public init(dic: Dictionary<Key, Value> = Dictionary<Key,Value>(), getter: @escaping (Key) -> Value) {
        self.dic = dic
        self.getter = getter
    }
}

public extension Result {
    struct LazyDic<Key:Hashable> {
        public var dic = Dictionary<Key,Success>()
        public var getter : (Key) -> R<Success>
        
        public init(dic: Dictionary<Key, Success> = Dictionary<Key,Success>(), getter: @escaping (Key) -> R<Success>) {
            self.dic = dic
            self.getter = getter
        }
    }
}
