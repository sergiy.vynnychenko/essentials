#if os(macOS)
import Foundation
import CryptoKit

public extension Data {
    func encrypted(password: String) -> R<Data> {
        CryptoUks.encript(with: SymmetricKey(password: password), data: self)
    }
    
    func decrypted(password: String) -> R<Data> {
        CryptoUks.decriptData(with: SymmetricKey(password: password), encryptedData: self)
    }
    
    func encrypted(key: SymmetricKey) -> R<Data> {
        CryptoUks.encript(with: key, data: self)
    }
    
    func decrypted(key: SymmetricKey) -> R<Data> {
        CryptoUks.decriptData(with: key, encryptedData: self)
    }
}
#endif
