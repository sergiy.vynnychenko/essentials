#if os(macOS)

import Foundation
import CryptoKit

@available(macOS 11.0, *)
public class FileCrypted {
    public let url: URL
    private let key: SymmetricKey
    
    public init(url: URL, key: SymmetricKey) {
        self.url = url
        self.key = key
    }
    
    public func exists() -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: url.path)
    }
    
    public func setContentR(_ str: String) -> R<()> {
        CryptoUks.encript(with: key, string: str)
            .flatMap{ data in
                Result{ try data.write(to: url, options: [.atomic, .completeFileProtection]) }
            }
    }
    
    public func getContentR() -> R<String> {
        Result{ try Data(contentsOf: url) }
            .flatMap { data in
                CryptoUks.decriptString(with: key, encryptedData: data)
            }
    }
}
#endif
