import Foundation
import CryptoKit

public struct CryptoUks {
    public static func encript(with key: SymmetricKey, string: String) -> R<Data> {
        return string
            .data(using: .utf8)
            .asNonOptional
            .flatMap {
                CryptoUksBase.encryptData(data: $0, key: key)
            }
    }
    
    public static func decriptString(with key: SymmetricKey, encryptedData: Data) -> R<String> {
        CryptoUksBase.decryptData(ciphertext: encryptedData, key: key)
            .map { decryptedData in
                String(data: decryptedData, encoding: .utf8)!
            }
    }
    
    public static func encript(with key: SymmetricKey, data: Data) -> R<Data> {
        CryptoUksBase.encryptData(data: data, key: key)
    }
    
    public static func decriptData(with key: SymmetricKey, encryptedData: Data) -> R<Data> {
        CryptoUksBase.decryptData(ciphertext: encryptedData, key: key)
    }
}

fileprivate struct CryptoUksBase {
    static func encryptData(data: Data, key: SymmetricKey) -> R<Data> {
        Result {
            try AES.GCM.seal(data, using: key)
        }
        .flatMap {
            $0.combined.asNonOptional
        }
    }
    
    static func decryptData(ciphertext: Data, key: SymmetricKey) -> R<Data> {
        Result {
            try AES.GCM.SealedBox(combined: ciphertext)
        }
        .flatMap { sealedBox in
            Result {
                try AES.GCM.open(sealedBox, using: key)
            }
        }
    }
}
