#if os(macOS)
import Foundation
import CryptoKit
import CommonCrypto

public extension SymmetricKey {
    init?(base64Encoded base64EncodedKeyString: String) {
        guard let keyData = Data(base64Encoded: base64EncodedKeyString) else { return nil }
        self.init(data: keyData)
    }
    
    var base64: String {
        self.withUnsafeBytes { body in
            Data(body).base64EncodedString()
        }
    }
    
    init(password: String) {
        let macAddr = Network.macAddress().replace(of: ":", to: "")
        
        self.init(password: password, withSalt: Data(macAddr.utf8) )
    }
    
    init(password: String, withSalt salt: Data) {
        self.init(data: makeKey(forPassword: password, withSalt: salt))
    }
}

/////////////////////
///HELPERS
/////////////////////
fileprivate func makeKey(forPassword password: String, withSalt salt: Data) -> Data {
    let passwordArray = password.utf8.map(Int8.init)
    let saltArray = Array(salt)
    let keySize = 32
    
    var derivedKey = Array<UInt8>(repeating: 0, count: keySize)
    
    // All the crazy casting because CommonCryptor hates Swift
    let algorithm    = CCPBKDFAlgorithm(kCCPBKDF2)
    let prf          = CCPseudoRandomAlgorithm(kCCPRFHmacAlgSHA1)
    let pbkdf2Rounds = UInt32(10000)
    
    let result = CCCryptorStatus(
        CCKeyDerivationPBKDF(
            algorithm,
            passwordArray, passwordArray.count,
            saltArray,     saltArray.count,
            prf,           pbkdf2Rounds,
            &derivedKey,   keySize)
    )
    guard result == CCCryptorStatus(kCCSuccess) else {
        fatalError("SECURITY FAILURE: Could not derive secure password (\(result))")
    }
    return Data(derivedKey)
}
#endif
