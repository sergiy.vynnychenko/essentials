#if os(macOS)
import Foundation

public struct CryptoDictFileID {
    let path : String
    let pass: String
    let debug: Bool = true
    
    public init(path: String, pass: String) {
        self.path = path.skippingLastSlash
        self.pass = pass
        
        makeSureFileExist(url: URL(fileURLWithPath: path) )
    }
}

public extension CryptoDictFileID {
    func set(key: String, value: String?) -> R<Void> {
        let dictR = debug ? dictFromFile : dictFromFileNonErorrable
        
        return dictR()
            .map { dict -> Dictionary<String,String> in
                var dict = dict
                dict[key] = value
                return dict
            }
            .flatMap { dict -> R<String> in dict.asJson() }
            .flatMap { json -> R<Data> in json.data(using: .utf8).asNonOptional }
            .flatMap {
                $0.encrypted(password: pass)
            }
            .flatMap { data in
                data.write(url: URL(fileURLWithPath: path) )
            }
            .map { _ -> () in () }
    }
    
    func get(key: String) -> R<String> {
        let dictR = debug ? dictFromFile : dictFromFileNonErorrable
        
        return dictR()
            .map {
                $0[key]
            }
            .flatMap { value -> R<String> in
                value.asNonOptional
            }
    }
}

/////////////////////
///HELPERS
//////////////////////
private extension CryptoDictFileID {
    private func dictFromFile() -> R<Dictionary<String,String>> {
        Result { try Data(contentsOf: URL(fileURLWithPath: path)) }
            .flatMap { $0.decrypted(password: pass) }
            .map { String(decoding: $0, as: UTF8.self) }
            .flatMap { json -> R<Dictionary<String,String>> in json.decodeFromJson(type: Dictionary<String,String>.self) }
    }
    
    private func dictFromFileNonErorrable() -> R<Dictionary<String,String>> {
        dictFromFile()
            .flatMapError{ _ -> R<Dictionary<String,String>> in
                return .success([:])
            }
    }
}

private extension CryptoDictFileID {
    func makeSureFileExist(url: URL) {
        if !url.exists {
            let makeSureExistR = Dictionary<String,String>()
                .asJson()
                .flatMap { json in json.data(using: .utf8).asNonOptional }
                .flatMap { data in data.encrypted(password: pass) }
                .flatMap { data in data.write(url: URL(fileURLWithPath: path) ) }
            
            if debug {
                makeSureExistR
                    .onFailure { printDbg($0) }
            }
            
            let _ = makeSureExistR.maybeSuccess
        }
    }
}
#endif
