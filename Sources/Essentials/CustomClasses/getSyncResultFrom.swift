import Foundation

@available(macOS 10.15, *)
private final class RunBlocking<T, Failure: Error> {
    fileprivate var value = LockedVar<Result<T, Failure>?>(nil)
    fileprivate var pool = FSPool(queue: .global(qos: .userInitiated))
}

@available(macOS 10.15, *)
extension RunBlocking where Failure == Never {
    func runBlocking(_ operation: @Sendable @escaping () async -> T) throws -> T {
        switch pool.future(operation).wait() {
        case let .success(value):
            return value
        case let .failure(error):
            throw error
        }
    }
}

@available(macOS 10.15, *)
extension RunBlocking where Failure == Error {
    func runBlocking(_ operation: @Sendable @escaping () async throws -> T) throws -> T {
        switch pool.future(operation).wait() {
        case let .success(value):
            return value
        case let .failure(error):
            throw error
        }
    }
}

/// ```
/// //Sample of usage
/// let html = getSyncResultFrom {
///    let loader = await MyInternetLoader()
///
///    return try? await loader.getHTML(from: URL(string: url)!)
/// }
@available(macOS 10.15, *)
public func getSyncResultFrom<T>(@_implicitSelfCapture _ operation: @Sendable @escaping () async -> T) throws -> T {
    try RunBlocking().runBlocking(operation)
}


@available(macOS 10.15, *)
public func getSyncResultFromR<T>(@_implicitSelfCapture _ operation: @Sendable @escaping () async -> T) -> R<T> {
    do {
        let tmp: T = try getSyncResultFrom{
            await operation()
        }
        
        return .success(tmp)
    } catch {
        return .failure(error)
    }
}





/// ```
/// //Sample of usage
/// let html = getSyncResultFrom {
///    let loader = await MyInternetLoader()
///
///    return try? await loader.getHTML(from: URL(string: url)!)
/// }
@available(macOS 10.15, *)
public func getSyncResultFrom<T>(@_implicitSelfCapture _ operation: @Sendable @escaping () async throws -> T) throws -> T {
    try RunBlocking().runBlocking(operation)
}
