import Foundation
import CoreFoundation

@available(macOS 10.15, *)
public class Stopwatch {
    public var elapsedS : Double {
        guard let startTime = startTime else { return 0 }
        
        if isGoing {
            return (CFAbsoluteTimeGetCurrent() - startTime )
        } else if let endTime = endTime {
            return (endTime - startTime)
        }
        
        return 0
    }
    
    public var elapsedMs: Double {
        return elapsedS * 1000
    }
    
    private var startTime : CFAbsoluteTime? = nil
    private var endTime : CFAbsoluteTime? = nil
    private var isGoing = false
    
    public init () { }
    
    public func start(_ str : String? = nil) -> Stopwatch {
        if let str = str {
            printDbg(str, displayDetails: false)
        }
        
        if startTime == nil {
            startTime = CFAbsoluteTimeGetCurrent()
            isGoing = true
            return self
        }
        
        isGoing = true
        return self
    }
    
    public func restart(_ str : String? = nil) -> Stopwatch {
        startTime = nil
        return start(str)
    }
    
    public func stop() {
        endTime = CFAbsoluteTimeGetCurrent()
        isGoing = false
    }
    
    public func reset() {
        startTime = nil
        endTime = nil
        isGoing = false
    }
    
    public func printS(_ str : String? = nil) {
        printDbg( str == nil ? "\(elapsedS) sec" : "\(str!): \(elapsedS) sec", displayDetails: false)
    }
    
    public func printMs(_ str : String? = nil){
        printDbg( str == nil ? "\(elapsedS) ms" : "\(str!): \(elapsedMs) ms", displayDetails: false)
    }
}

///////////////////
/// OTHER | Garbage
//////////////////

public extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
