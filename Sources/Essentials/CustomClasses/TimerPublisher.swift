import Foundation
import Combine


/// TimerPublisher
///
/// Create:
/// ```
/// let timer = TimerPublisher(every: 0.3)
/// ```
/// Usage 1:
/// ```
///  SomeView()
///    .onReceive(timer) { _ in
///        //some code to run every 0.3
///
///        if ( needToCancel ) {
///            timer.stopTimerPublisher()
///        }
///    }
/// ```
///
/// Usage 2:
/// ```
///  import Combine
///
///  class MyViewModel {
///      @Published var timerPublisher = TimerPublisher(every: 5)
///      private var timerPublisherCancellable: AnyCancellable?
///
///      init() {
///          timerPublisherCancellable = timerPublisher.sink { _ in
///              print("test")
///          }
///      }
///  }
/// ```
@available(macOS 10.15, *)
@available(iOS 15, *)
public func TimerPublisher(every: CGFloat) -> Publishers.Autoconnect<Timer.TimerPublisher> {
    Timer.publish(every: every, on: .main, in: .common).autoconnect()
}

@available(macOS 10.15, *)
@available(iOS 15, *)
public extension Publishers.Autoconnect where Upstream == Timer.TimerPublisher {
    func stopTimerPublisher() {
        self.upstream.connect().cancel()
    }
}
