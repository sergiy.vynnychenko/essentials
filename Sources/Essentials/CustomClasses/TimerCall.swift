/* Sample of usage:

*/



import Foundation

public class TimerCall {
    private var type: TimerCallType
    private var action: () -> ()
    private(set) var stopCondition: () -> Bool = { return false }
    
    private var timer: Timer? = nil { willSet { timer?.invalidate() } }
    
    ///```
    /////WillSet IS IMPORTANT!
    ///private(set) var timer: TimerCall? { willSet { timerCall?.invalidate() } }
    ///
    ///init() {
    ///    //1
    ///    self.timer = TimerCall(.continious(interval: 1)) {
    ///        //someAct
    ///    }
    ///
    ///    //2
    ///    self.timer = TimerCall(.oneTime(interval: 1)) {
    ///         //someAct
    ///    }
    ///
    ///    //3
    ///    self.timer = TimerCall(.stopCondition(interval: 0.3, cond: { someCondition })) {
    ///        //someAct
    ///    }
    ///}
    ///```
    public init(_ type: TimerCallType, stopCondition: @escaping () -> Bool = { return false }, action: @escaping () -> Void) {
        self.action = action
        self.type = type
        
        switch type {
        case .oneTime(let interval):
            timer = Timer.scheduledTimer(timeInterval: interval,
                                 target: self,
                                 selector: #selector(self.updateCounting),
                                 userInfo: nil,
                                 repeats: false)
            
        case .continious(let interval):
            timer = Timer.scheduledTimer(timeInterval: interval,
                                 target: self,
                                 selector: #selector(self.updateCounting),
                                 userInfo: nil,
                                 repeats: true)
            
        case .stopCondition(let interval, _):
            timer = Timer.scheduledTimer(timeInterval: interval,
                                 target: self,
                                 selector: #selector(self.updateCounting),
                                 userInfo: nil,
                                 repeats: true)
        }
    }
    
    @objc func updateCounting() {
        guard timer != nil else { return }
        
        if case let .stopCondition(_ , condition) = type {
            if condition() == true {
                invalidate()
            }
        }
        
        action()
        
        if case .oneTime(_) = type {
            invalidate()
        }
    }
    
    public func invalidate() {
        timer?.invalidate()
        timer = nil
        action = { }
    }
}

public enum TimerCallType {
    case continious(interval: TimeInterval)
    case oneTime(interval: TimeInterval)
    case stopCondition(interval: TimeInterval, cond: () -> Bool )
}
