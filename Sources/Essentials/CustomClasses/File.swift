#if os(macOS)

import Foundation

public class File {
    public let url: URL
    let encoding = String.Encoding.utf8
    
    public init(url: URL) {
        self.url = url
    }
    
    public func exists() -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: url.path)
    }
    
    public func getContent() -> String? {
        guard let content = try? String(contentsOfFile: url.path)
        else { return nil }
        
        return content.last == "\n" ? content.dropLast(1).asStr() : content
    }
    
    public func setContent(_ str: String) throws {
        let strToWrite = str.last == "\n" ? str : str.appending("\n")
        
        try strToWrite.write(to: url, atomically: true, encoding: encoding)
    }
    
    public func setContentR(_ str: String) -> R<()> {
        let strToWrite = str.last == "\n" ? str : str.appending("\n")
        
        return Result { try strToWrite.write(to: url, atomically: true, encoding: encoding) }
    }
    
    public func append(_ str: String) {
        if let fileUpdater = try? FileHandle(forUpdating: url) {
            fileUpdater.seekToEndOfFile()
            
            let strToWrite = str.last == "\n" ? str : str.appending("\n")
            
            fileUpdater.write(strToWrite.data(using: encoding)!)
            fileUpdater.closeFile()
        }
    }
    
    public func delete() {
        if exists() {
            delete(url.path)
        }
    }
}

fileprivate extension File {
    func delete(_ path : String, silent: Bool = true) {
        if !silent {
            print("FS: going to delete file: \(path)")
        }
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(atPath: path)
        } catch let error {
            if !silent {
                print("FS: cant delete \(path)")
                print(error)
            }
        }
    }
}

public class FileStream {
    public let url: URL
    private let filePointer:UnsafeMutablePointer<FILE>
    
    // a pointer to a null-terminated, UTF-8 encoded sequence of bytes
    private var lineByteArrayPointer: UnsafeMutablePointer<CChar>? = nil
    
    // the smallest multiple of 16 that will fit the byte array for this line
    private var lineCap: Int = 0
    
    private var bytesRead = -2
    
    public init?(url: URL) {
        self.url = url
        
        guard let fPointer = fopen(url.path,"r") else { return nil }
        
        self.filePointer = fPointer
    }
    
    deinit {
        fclose(filePointer)
    }
    
    public func readLn() -> String? {
        if bytesRead == -2 {
            bytesRead = getline(&lineByteArrayPointer, &lineCap, filePointer)
        }
        else if bytesRead < 1 {
            return nil
        }
        
        let lineAsString = String.init(cString:lineByteArrayPointer!)
        
        bytesRead = getline(&lineByteArrayPointer, &lineCap, filePointer)
        
        return lineAsString
    }
}

public class FileOld {
    var file : FileHandle?
    
    let encoding : UInt = String.Encoding.utf8.rawValue
    
    // stuff for reading
    let delimiter : String = "\n"
    var delimiterData : Data
    let chunkSize : Int = 4096
    let buffer : NSMutableData?
    var atEof : Bool = false
    var bytes : UInt64 = 0
    
    //////////////////////////////////////////////////////////////////////////////////////
    
    public var inited : Bool    { return file != nil }
    public var notInited : Bool { return file == nil }
    
    ////////////////////////////////////////////////////////////////////////////////////////
        
    public init(w: String){
        let fileManager = FileManager.default
        
        if (fileManager.fileExists(atPath: w))
        {
            do {
                try fileManager.removeItem(atPath: w)
            } catch _ {
            }
        }
        
        fileManager.createFile(atPath: w, contents: nil, attributes: nil)
        
        file = FileHandle(forWritingAtPath: w)
        
        // this is not necessary for writing, but must be in constructor
        delimiterData = delimiter.data(using: String.Encoding(rawValue: encoding))!
        buffer = NSMutableData(capacity: chunkSize)
    }
    
    public init(r: URL) {
        file = try? FileHandle(forReadingFrom: r)
        // delimiter usually is "\n"
        delimiterData = delimiter.data(using: String.Encoding(rawValue: encoding))!
        // buffer for reading
        buffer = NSMutableData(capacity: chunkSize)
        
        bytes = file!.seekToEndOfFile()
        file!.seek(toFileOffset: 0)
    }
    
    deinit {
        file?.closeFile()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    @discardableResult
    public func write(_ text : String) -> Int {
        let data = text.data(using: String.Encoding(rawValue: encoding))!
        file?.write(data)
        
        return data.count
    }
    
    public func readln() -> String? {
        if atEof {
            return nil
        }
        
        // Read data chunks from file until a line delimiter is found:
        var range = buffer?.range(of: delimiterData, options: NSData.SearchOptions(rawValue: 0), in: NSMakeRange(0, buffer!.length))
        while range!.location == NSNotFound {
            let tmpData : Data? = file!.readData(ofLength: chunkSize)
            if tmpData == nil || tmpData!.count == 0 {
                // EOF or read error.
                atEof = true
                if buffer!.length > 0 {
                    // Buffer contains last line in file (not terminated by delimiter).
                    let line = NSString(data: buffer! as Data, encoding: encoding);
                    buffer!.length = 0
                    
                    return line as String?
                }
                // No more lines.
                return nil
            }
            buffer!.append(tmpData!)
            range = buffer!.range(of: delimiterData, options: NSData.SearchOptions(rawValue: 0), in: NSMakeRange(0, buffer!.length))
        }
        // Convert complete line (excluding the delimiter) to a string:
        let line = NSString(data: buffer!.subdata(with: NSMakeRange(0, range!.location)),
            encoding: encoding)
        // Remove line (and the delimiter) from the buffer:
        buffer!.replaceBytes(in: NSMakeRange(0, range!.location + range!.length), withBytes: nil, length: 0)
        
        return line as String?
    }
    
    public func flush() {
        file?.synchronizeFile()
    }
    
    public func size() -> Int {
        return Int(bytes)
    }
    
    public func progress() -> Float {
        if file == nil {
            return 0
        }
        let file_sz : Float = Float(bytes);
        let location : Float = Float(file!.offsetInFile);
        
        return location / file_sz;
    }
}
#endif
