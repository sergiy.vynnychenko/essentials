import Foundation

public class Chron {
    public static var shared = Chron()
    
    private init() { }
    
    private var timers: [String: TimerPair] = [:]
    
    public func scheduleAsyncFake(_ type: ScheduleType, action: @escaping () async ->() ) -> String {
        func mySyncFunction() {
            Task {
                await action()
            }
        }
        
        return schedule(type, action: mySyncFunction )
    }
    
    //@available(iOS 16.0, *)
    //@available(macOS 12.0, *)
    public func schedule(_ type: ScheduleType, action: @escaping ()->() ) -> String {
        let token = UUID().uuidString
        
        switch type {
        case .every(let hrs, let mins, let sec, let skipFirst):
            if skipFirst == false {
                action()
            }
            
            let timer = TimerPair(hrs: hrs, mins: mins, sec: sec, action: action)
            timers[token] = timer
            break
            
        case .everyDayBeginning: // HALF TESTED
            let diff = (Date.tomorrow - Date()).rounded(.up)
            
            //printDbg( "schedulet to: " + Date().adding(.second, value: Int(diff) ).formatted() )
            
            DispatchQueue.main.asyncAfter(deadline: .now() + diff) {
                action()
                
                let timer = TimerPair(hrs: 24, mins: 0, sec: 0, action: action)
                self.timers[token] = timer
            }
            
            break
            
        case .everyDay(let hrs, let mins, let sec): // HALF TESTED
            self.timers[token] = TimerPair(empty: true)
            
            let potentialDate1 = Date(hrs: hrs, mins: mins, sec: sec, nanoSec: 0)
            let potentialDate2 = potentialDate1.adding(.day, value: 1)
            let date = potentialDate1 > Date() ? potentialDate1 : potentialDate2
            
            let diff = (date - Date() + 0.1).rounded(.up)
            
            //printDbg( "schedulet to: " + Date().adding(.second, value: Int(diff) ).formatted() )
            
            DispatchQueue.main.asyncAfter(deadline: .now() + diff) {
                if let _ = self.timers[token] {
                    action()
                    
                    let timer = TimerPair(oncePerDayFromNow: true, action: action)
//                    let timer = TimerPair(hrs: 0, mins: 0, sec: 10, action: action)
                    self.timers[token] = timer
                }
            }
            
            break
            
        case .singleTimeAt(let hrs, let mins, let sec): //TESTED
            self.timers[token] = TimerPair(empty: true)
            
            let potentialDate1 = Date(hrs: hrs, mins: mins, sec: sec, nanoSec: 0)
            let potentialDate2 = potentialDate1.adding(.day, value: 1)
            let date = potentialDate1 > Date() ? potentialDate1 : potentialDate2
            
            let diff = date - Date()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + diff + 0.1) {
                if let _ = self.timers[token] {
                    action()
                    
                    self.timers.remove(key: token)
                }
            }
        }
        
        return token
    }
    
    public func cancel(token: String) {
        if let pair = timers[token] {
            pair.canceled = true
            pair.timer?.invalidate()
            pair.timer = nil
            pair.action = { }
            
            self.timers.remove(key: token)
        }
    }
    
    public func cancelAll() {
        timers.keys.forEach {
            cancel(token: $0)
        }
    }
}

private class TimerPair {
    var timer: Timer?
    var action: () -> ()
    var canceled: Bool = false
    
    init(oncePerDayFromNow: Bool, action: @escaping () -> () ) {
        if oncePerDayFromNow == false {
            fatalError("incorrect parameter!")
        }
        
        self.action = action
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(days: 1, hrs: 0, mins: 0, sec: 0),
                             target: self,
                             selector: #selector(self.updateCounting),
                             userInfo: nil,
                             repeats: true)
    }
    
    init(hrs: Int, mins: Int, sec: Int, action: @escaping () -> () ) {
        let interval = TimeInterval(days: 0, hrs: hrs, mins: mins, sec: sec)
        
        self.action = action
        
        timer = Timer.scheduledTimer(timeInterval: interval,
                             target: self,
                             selector: #selector(self.updateCounting),
                             userInfo: nil,
                             repeats: true)
    }
    
    init(empty: Bool ) {
        action = { }
    }
    
    @objc private func updateCounting() {
        if !canceled {
            action()
        }
    }
}

public enum ScheduleType {
    case every(hrs: Int, mins: Int, sec: Int, skipFirst: Bool)
    case everyDayBeginning
    case everyDay(hrs: Int, mins: Int, sec: Int) // HALF TESTED
    case singleTimeAt(hrs: Int, mins: Int, sec: Int) // tested!!!!!
}

public extension Date {
    init(hrs: Int, mins: Int, sec: Int, nanoSec: Int) {
        let date = Date()
        
        self.init(calendar: .current,
                  timeZone: .current,
                  era: date.era,
                  year: date.year,
                  month: date.month,
                  day: date.day,
                  hour: hrs,
                  minute: mins,
                  second: sec,
                  nanosecond: nanoSec)!
    }
}
