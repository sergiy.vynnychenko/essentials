import Foundation

public func printDbg( _ items: Any..., separator: String = " ", terminator: String = "\n", _ lineNumber: Int = #line, displayDetails: Bool = true) {
    #if DEBUG
    if displayDetails {
        func matchRegex(_ matcher: String,  string : String) -> String? {
            let regex = try! NSRegularExpression(pattern: matcher, options: [])
            let range = NSRange(string.startIndex ..< string.endIndex, in: string)
            guard let textCheckingResult = regex.firstMatch(in: string, options: [], range: range) else {
                return nil
            }
            return (string as NSString).substring(with:textCheckingResult.range(at:1)) as String
        }
        
        func singleMatchRegex(_ matcher: String,  string : String) -> String? {
            let regex = try! NSRegularExpression(pattern: matcher, options: [])
            let range = NSRange(string.startIndex ..< string.endIndex, in: string)
            let matchRange = regex.rangeOfFirstMatch(in: string, range: range)
            if matchRange == NSMakeRange(NSNotFound, 0) {
                return nil
            }
            return (string as NSString).substring(with: matchRange) as String
        }
        
        var string = Thread.callStackSymbols[1]
        
        if let idxDollar = string.firstIndex(of: "$") {
            string = String(string.suffix(from: idxDollar))
            
            if let appNameLenString = matchRegex(#"\$s(\d*)"#, string: string) {
                string = String(string.dropFirst(appNameLenString.count + 2))
                
                if let appNameLen = Int(appNameLenString) {
                    let appName = singleMatchRegex(".{\(appNameLen)}", string: string)
                    
                    string = String(string.dropFirst(appNameLen))
                    
                    if let classNameLenString = singleMatchRegex(#"\d*"#, string: string),
                       let classNameLen = Int(classNameLenString)
                    {
                        string = String(string.dropFirst(classNameLenString.count))
                        
                        if let className = singleMatchRegex(".{\(classNameLen)}", string: string) {
                            string = String(string.dropFirst(classNameLen))
                            
                            if let methodNameLenString = matchRegex(#".(\d*)"#, string: string) {
                                if let methodNameLen = Int(methodNameLenString){
                                    string = String(string.dropFirst(methodNameLenString.count + 1))
                                    
                                    if let methodName = singleMatchRegex(".{\(methodNameLen)}", string: string) {
                                        let _ = appName
                                        
                                        print("[DBG] \(className).\(methodName)() [\(lineNumber)]: ", terminator: "")
                                    }
                                }
                            } else {
                               print("[DBG] \(className) [\(lineNumber)]: ", terminator: "")
                            }
                        }
                    }
                }
            }
        }
        
    } else {
        print("[DBG]: ", terminator: "")
    }
    
    print(items, separator: separator, terminator: terminator)
    #endif
}
