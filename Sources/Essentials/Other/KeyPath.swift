
import Foundation

public func copy<T, each Value>(to dest: inout T, from src: T, keyPaths: (repeat WritableKeyPath<T, each Value>)) {
    repeat copy(to: &dest, from: src, keyPath: each keyPaths)
}

private func copy<T, Value>(to dest: inout T, from src: T, keyPath: WritableKeyPath<T, Value>) {
    dest[keyPath: keyPath] = src[keyPath: keyPath]
}
