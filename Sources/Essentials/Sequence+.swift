import Foundation

public extension Sequence {
    /// groupBy
    ///
    /// Usage:
    /// ```
    /// arr.group(by: { $0.propertyName })
    /// ```
    func group<T: Hashable>(by key: (Iterator.Element) -> T) -> [T : [Self.Element]] {
        return Dictionary(grouping: self, by: key )
    }
    
    /// groupBy
    ///
    /// Usage:
    /// ```
    /// arr.group(by: { $0.propertyName }, values: \.propertyName2 )
    /// ```
    func group<T: Hashable>(by key: (Iterator.Element) -> T, value keyPath: KeyPath<Element, T> ) -> [T : [T]] {
        return Dictionary(grouping: self, by: key )
            .mapValues{ $0.map{ $0[keyPath: keyPath] } }
    }
}
