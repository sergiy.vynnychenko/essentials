import Foundation
import AVFoundation

public class AudioPlayerManager {
    private var players: [AVAudioPlayer] = []
    
    public var isPlaying: Bool { players.atLeastOneSatisfy{ $0.isPlaying } }
    
    public init() { }
    
    private func cleanup() {
        players = players.filter{ $0.isPlaying }
    }
    
    public func playSound(fileName: String, ext: String = "mp3", volume: Float, repeatForever: Bool = false) -> Void {
        guard let url = Bundle.main.url(forResource: fileName, withExtension: ext)
        else { return }
        
        playSound(url: url, volume: volume, repeatForever: repeatForever)
    }
    
    public func playSound(url: URL, volume: Float, repeatForever: Bool = false) -> Void {
        cleanup()
        
        do {
            players.append(try AVAudioPlayer(contentsOf: url))
            players.last?.volume = volume
            players.last?.prepareToPlay()
            players.last?.play()
            players.last?.numberOfLoops = repeatForever ? -1 : 0
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    public func stopPlayingAll() {
        players.forEach{
            $0.stop()
        }
        
        players = []
    }
}
