
import Foundation

public struct FutureID : Equatable, Hashable {
    let id : UUID
    
    fileprivate init(){
        self.id = UUID()
    }
}

extension Flow {
    public class Future<Success> : FRetainer {
        public let id = FutureID()
        
        var result = LockedVar<R<Success>?>(nil)
        
        var retainer    = LockedVar<[Any]>([])
        let semaphore   = DispatchSemaphore(value: 0)
        let queue       : DispatchQueue
        let block       : (Future<Success>)->Void
        
        public var maybeSuccess: Success?     { result.read?.maybeSuccess }
        public var maybeFailure: Error?       { result.read?.maybeFailure }
        
        var completeHandlers    = LockedVar<[Handler<R<Success>>]>([])
        var successHandlers     = LockedVar<[Handler<Success>]>([])
        var errorHandlers       = LockedVar<[Handler<Error>]>([])
        
        public init(_ r: R<Success>) {
            self.queue = .global()
            self.block = { _ in }
            self.result.access { $0 = r }
            semaphore.signal()
        }
        
        public func clone() -> Future<Success> {
            Future(queue: self.queue, block: self.block)
        }
        
        // complete should be send manually
        public static func promise(queue: DispatchQueue = .global()) -> Future<Success> {
            return Future(queue: queue) { _ in }
        }
        
        public init(queue: DispatchQueue = .global(), block: @escaping (Future<Success>)->Void) {
            self.queue = queue
            self.block = block
        }
        
        public init(queue: DispatchQueue = .global(), block: @escaping ()->R<Success>) {
            self.queue = queue
            self.block = { promise in promise.complete(block()) }
        }
        
        public init(queue: DispatchQueue = .global(), _ operation: @Sendable @escaping () async -> Success) {
            self.queue = queue
            self.block = { promise in
                Task {
                    let task = Task(operation: operation)
                    let result = await task.result  // Result<Success,Never>
                    if case .success(let value) = result {
                        promise.complete(.success(value))
                    }
                }
            }
        }
        
        public init(queue: DispatchQueue = .global(), _ operation: @Sendable @escaping () async throws -> Success) {
            self.queue = queue
            self.block = { promise in
                Task {
                    let task = Task(operation: operation)
                    let result = await task.result
                    promise.complete(result)
                }
            }
        }
        
        
        //    deinit {
        //        print("deinit")
        //    }
        
        public func retain(_ obj: Any) {
            retainer.access { $0.append(obj) }
        }
        
        func retain<T>(at: Flow.Future<T>) -> Self {
            at.retain(self)
            return self
        }
        
        func retain(at: FSPool) -> Flow.Future<Success> {
            at.retain(self)
        }
        
        public func failing(into pool: FSPool) -> Flow.Future<Success> {
            self.onFailure { [weak pool] error in
                pool?.append(error: error)
            }
            return self
        }
        
        public func complete(_ result: R<Success>) {
            guard self.result.read == nil else { return }
            
            self.result.access {
                $0 = result
            }
            
            semaphore.signal()
            
            completeHandlers.access {
                for handler in $0 {
                    handler.schedule(result)
                }
            }
            
            switch result {
            case let .success(success):
                successHandlers.access {
                    for handler in $0 {
                        handler.schedule(success)
                    }
                }
            case let .failure(error):
                errorHandlers.access {
                    for handler in $0 {
                        handler.schedule(error)
                    }
                }
            }
        }
        
        public func wait() -> R<Success> {
            if let r = self.result.read {
                return r
            }
            
            initiate()
            semaphore.wait()
            if let r = self.result.read {
                return r
            }
            return .failure(WTF("F.result can't be nil WTF"))
        }
        
        var isInitiated = LockedVar<Bool>(false)
        
        func initiate() {
            queue.async { [weak self] in
                self?.isInitiated.access { [weak self] isInitiated in
                    guard let me = self else { return }
                    
                    if !isInitiated {
                        me.block(me)
                        isInitiated = true
                    }
                }
            }
            
        }
    }
}
