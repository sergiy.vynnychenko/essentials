
import Foundation

public extension Flow.Future {
    static func succeeded(_ success: Success) -> Flow.Future<Success> {
        return Flow.Future(.success(success))
    }
    
    static func failed(_ error: Error) -> Flow.Future<Success> {
        return Flow.Future(.failure(error))
    }
    
    
    // designed to be used with SwiftUI @Published variables, so main thread is default
    
    // Assign Success
    @discardableResult
    func assign<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Success>, queue : DispatchQueue = .main) -> Self {
        onSuccess(queue: queue) { [weak context] success in
            guard let context = context else { return }
            context[keyPath: keyPath] = success
        }
    }
    
    // Assign Success?
    @discardableResult
    func assign<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Success?>, queue : DispatchQueue = .main) -> Self {
        onSuccess(queue: queue) { [weak context] success in
            guard let context = context else { return }
            context[keyPath: keyPath] = success
        }
    }
    
    // Append to [Success]
    @discardableResult
    func append<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, [Success]>, queue : DispatchQueue = .main) -> Self {
        onSuccess(queue: queue) { [weak context] success in
            guard let context = context else { return }
            context[keyPath: keyPath].append(success)
        }
    }
    
    // Assign Error
    @discardableResult
    func assignFailure<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Error>, queue : DispatchQueue = .main) -> Self {
        onFailure(queue: queue) { [weak context] error in
            guard let context = context else { return }
            context[keyPath: keyPath] = error
        }
    }
    
    // Assign Error?
    @discardableResult
    func assignFailure<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Error?>, queue : DispatchQueue = .main) -> Self {
        onFailure(queue: queue) { [weak context] error in
            guard let context = context else { return }
            context[keyPath: keyPath] = error
        }
    }
    
    // Append to [Error]
    @discardableResult
    func appendFailure<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, [Error]>, queue : DispatchQueue = .main) -> Self {
        onFailure(queue: queue) { [weak context] error in
            guard let context = context else { return }
            context[keyPath: keyPath].append(error)
        }
    }
    
    @discardableResult
    func completing(future another: Flow.Future<Success>, queue : DispatchQueue? = nil, retain: Bool = false) -> Self {
        if retain {
            onComplete(queue: queue) { result in
                another.complete(result)
            }
        } else {
            onComplete(queue: queue) { [weak another] result in
                another?.complete(result)
            }
        }
        return self
    }
    
    @discardableResult
    func updating(signal another: Flow.Signal<Success>, queue : DispatchQueue? = nil, retain: Bool = false) -> Self {
        if retain {
            onSuccess(queue: queue) { success in
                another.update(success)
            }
        } else {
            onSuccess(queue: queue) { [weak another] success in
                another?.update(success)
            }
        }
        return self
    }
    
    @discardableResult
    func updatingError(signal another: Flow.Signal<Error>, queue : DispatchQueue? = nil, retain: Bool = false) -> Self {
        if retain {
            onFailure(queue: queue) { error in
                another.update(error)
            }
        } else {
            onFailure(queue: queue) { [weak another] error in
                another?.update(error)
            }
        }
        return self
    }
    
    @discardableResult
    func onComplete<C:AnyObject>(context: C, queue : DispatchQueue? = nil, block: @escaping (C,R<Success>)->Void) -> Self {
        return onComplete(queue: queue) { [weak context] result in
            guard let context = context else { return }
            block(context,result)
        }
    }
    
    @discardableResult
    func onComplete(queue: DispatchQueue? = nil, block: @escaping (R<Success>)->Void) -> Self {
        let handler = Handler(queue: queue ?? self.queue, block: block)
        completeHandlers.access {
            $0.append(handler)
        }
        
        if let result = self.result.read {
            handler.schedule(result)
            return self
        }
        
        initiate()
        return self
    }
    
    @discardableResult
    func onSuccess<C:AnyObject>(context: C, queue : DispatchQueue? = nil, block: @escaping (C,Success)->Void) -> Self {
        return onSuccess(queue: queue) { [weak context] success in
            guard let context = context else { return }
            block(context,success)
        }
    }

    @discardableResult
    func onSuccess(queue: DispatchQueue? = nil, block: @escaping (Success)->Void) -> Self {
        let handler = Handler(queue: queue ?? self.queue, block: block)
        successHandlers.access {
            $0.append(handler)
        }
        
        if let result = self.result.read {
            if case .success(let success) = result {
                handler.schedule(success)
            }
            return self
        }
        
        initiate()
        return self
    }
    
    @discardableResult
    func onFailure<C:AnyObject>(context: C, queue : DispatchQueue? = nil, block: @escaping (C,Error)->Void) -> Self {
        return onFailure(queue: queue) { [weak context] error in
            guard let context = context else { return }
            block(context,error)
        }
    }
    
    @discardableResult
    func onFailure(queue: DispatchQueue? = nil, block: @escaping (Error)->Void) -> Self {
        let handler = Handler(queue: queue ?? self.queue, block: block)
        errorHandlers.access {
            $0.append(handler)
        }
        
        if let result = self.result.read {
            if case .failure(let error) = result {
                handler.schedule(error)
            }
            return self
        }
        
        initiate()
        return self
    }
}

internal class Handler<T> {
    let queue : DispatchQueue
    let block: (T)->Void
    
    init(queue : DispatchQueue, block: @escaping (T) -> Void) {
        self.queue = queue
        self.block = block
    }
    
    func schedule(_ result: T) {
        queue.async { [weak self] in
            self?.block(result)
        }
    }
}
