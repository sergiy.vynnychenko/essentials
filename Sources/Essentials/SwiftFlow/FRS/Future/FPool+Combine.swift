
import Foundation

public extension FSPool {
    func combine<T1,T2>(_ f1: Flow.Future<T1>, _ f2: Flow.Future<T2>, queue: DispatchQueue = .global()) -> Flow.Future<(T1,T2)> {
        let _combine = FCombine2(f1, f2)
        self.retain(_combine)
        return _combine.final
    }
    
    func combine<T1,T2,T3>(_ f1: Flow.Future<T1>, _ f2: Flow.Future<T2>, _ f3: Flow.Future<T3>, queue: DispatchQueue = .global()) -> Flow.Future<(T1,T2,T3)> {
        let _combine = FCombine3(f1, f2, f3)
        self.retain(_combine)
        return _combine.final
    }
}

private class FCombine2<T1, T2> {
    let queue = DispatchQueue(label: "Combine2.Serial")
    let final = Flow.Future<(T1,T2)>.promise()
    var success1: T1?
    var success2: T2?
    
    init(_ f1: Flow.Future<T1>, _ f2: Flow.Future<T2>) {
        f1
            .onSuccess(context: self, queue: queue) { me, success in me.success1 = success }
            .onFailure(context: self, queue: queue) { me, error   in me.final.complete(.failure(error))}
        
        f2
            .onSuccess(context: self, queue: queue) { me, success in me.success2 = success }
            .onFailure(context: self, queue: queue) { me, error   in me.final.complete(.failure(error))}
    }
    
    func tryComplete() {
        guard let s1 = success1 else { return }
        guard let s2 = success2 else { return }
        
        final.complete(.success((s1,s2)))
    }
}

private class FCombine3<T1, T2, T3> {
    let queue = DispatchQueue(label: "Combine3.Serial")
    let final = Flow.Future<(T1,T2,T3)>.promise()
    var success1: T1?
    var success2: T2?
    var success3: T3?
    
    init(_ f1: Flow.Future<T1>, _ f2: Flow.Future<T2>, _ f3: Flow.Future<T3>) {
        f1
            .onSuccess(context: self, queue: queue) { me, success in me.success1 = success }
            .onFailure(context: self, queue: queue) { me, error   in me.final.complete(.failure(error))}
        
        f2
            .onSuccess(context: self, queue: queue) { me, success in me.success2 = success }
            .onFailure(context: self, queue: queue) { me, error   in me.final.complete(.failure(error))}
        
        f3
            .onSuccess(context: self, queue: queue) { me, success in me.success3 = success }
            .onFailure(context: self, queue: queue) { me, error   in me.final.complete(.failure(error))}
    }
    
    func tryComplete() {
        guard let s1 = success1 else { return }
        guard let s2 = success2 else { return }
        guard let s3 = success3 else { return }
        
        final.complete(.success((s1,s2,s3)))
    }
}
