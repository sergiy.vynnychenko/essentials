
import Foundation

extension Flow.Future {
    public func map<NewSuccess>(queue : DispatchQueue = .global(), block: @escaping (Success) -> (NewSuccess)) -> Flow.Future<NewSuccess> {
        return Flow.Future<NewSuccess>(queue: self.queue) { [weak self] promise in
            self?.onSuccess(queue: .global()) { promise.complete(.success(block($0))) }
        }.retain(at: self)
    }
    
    
    public func map<NewSuccess>(queue : DispatchQueue = .global(), _ operation: @Sendable @escaping (Success) async -> NewSuccess) -> Flow.Future<NewSuccess> {
        Flow.Future<NewSuccess>(queue: self.queue) { [weak self] promise in
            self?.onSuccess(queue: .global()) { success in
                Task {
                    let task = Task { await operation(success) }
                    let result = await task.result  // Result<Success,Never>
                    if case .success(let value) = result {
                        promise.complete(.success(value))
                    }
                }
            }
        }.retain(at: self)
    }
    
    // flatMap throws await
    public func flatMap<NewSuccess>(queue : DispatchQueue = .global(), block: @escaping (Success) -> (R<NewSuccess>)) -> Flow.Future<NewSuccess> {
        Flow.Future<NewSuccess>(queue: self.queue) { [weak self] promise in
            self?.onSuccess(queue: .global()) { promise.complete(block($0)) }
        }.retain(at: self)
    }
    
    public func flatMap<NewSuccess>(queue : DispatchQueue = .global(), block: @escaping (Success) -> (Flow.Future<NewSuccess>)) -> Flow.Future<NewSuccess> {
        Flow.Future<NewSuccess>(queue: self.queue) { [weak self] promise in
            self?.onSuccess(queue: .global()) { promise.complete( block($0).wait() ) }
        }.retain(at: self)
    }
    
    public func flatMap<NewSuccess>(queue : DispatchQueue = .global(), _ operation: @Sendable @escaping (Success) async throws -> NewSuccess) -> Flow.Future<NewSuccess> {
        Flow.Future<NewSuccess>(queue: self.queue) { [weak self] promise in
            self?.onSuccess(queue: .global()) { success in
                Task {
                    let task = Task { try await operation(success) }
                    let result = await task.result
                    promise.complete(result)
                }
            }
        }.retain(at: self)
    }
}

func wait<Success,NewSuccess>(arg: Success, _ operation: @Sendable @escaping (Success) async throws -> NewSuccess) -> R<NewSuccess> {
    Flow.Future<NewSuccess>(queue: .global()) { promise in
        Task {
            let task = Task { try await operation(arg) }
            let result = await task.result
            promise.complete(result)
        }
    }.wait()
}
