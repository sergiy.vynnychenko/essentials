
import Foundation

// S for Signal

public extension Flow {
    class Signal<Update> : FRetainer {
        private var retainer    = LockedVar<[Any]>([])
        
        var update      = LockedVar<Update?>(nil)
        let queue       : DispatchQueue
        
        var updateHandlers     = LockedVar<[Handler<Update>]>([])
        
        public var currentValue : Update? { update.read }
        public func clearBuffer() { update.access { $0 = nil } }
        
        public init(queue: DispatchQueue, update: Update? = nil) {
            self.queue = queue
            if let update = update {
                self.update(update)
            }
        }
        
        public func update(_ upd: Update) {
            update.access {
                $0 = upd
            }
            
            for handler in updateHandlers.read {
                handler.schedule(upd)
            }
        }
        
        public func retain(_ obj: Any) {
            retainer.access {
                $0.append(obj)
            }
        }
    }
    
    
}

public extension Flow.Signal  {
    func startWith(_ upd: Update) -> Flow.Signal<Update> {
        self.update(upd)
        
        return self
    }
}
