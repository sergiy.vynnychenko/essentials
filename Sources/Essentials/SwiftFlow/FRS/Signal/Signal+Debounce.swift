//
//  File.swift
//  
//
//  Created by Sergiy Vynnychenko on 15.02.2025.
//

import Foundation

public extension Flow.Signal {
    func debounce(interval: Double) -> Flow.Signal<Update> {
        let helper = DebounceEventSourceHelper<Update>(type: Update.self,interval: interval, qos: .userInteractive)
        self.retain(helper)
        
        self.onUpdate(context: helper) { helper, update in
            helper.onUpdate(update: update)
        }
        
        return helper.destination
    }
}

private class DebounceEventSourceHelper<Update> {
    var nextUpdate = LockedVar<Update?>(nil)
    let queue: DispatchQueue
    var timer = LockedVar<DispatchSourceTimer?>(nil)
    var timerInterval: DispatchTimeInterval
    var destination: Flow.Signal<Update>
    
    init(type: Update.Type, interval: Double, qos: DispatchQoS.QoSClass) {
        
        self.destination = Flow.Signal(queue: .global(qos: qos))
        self.timerInterval = interval.dispatchInterval
        self.queue = DispatchQueue.init(label: "SwiftFlow.debounce Timer DispatchQueue", qos: .userInteractive)
    }
  
    func onUpdate(update: Update) {
        if timer.read == nil {
            sendNow(update: update)
            createTimer()
        } else {
            nextUpdate.access { $0 = update }
        }
    }
}

extension DebounceEventSourceHelper {
  private func sendNow(update: Update) {
      nextUpdate.access { $0 = nil }
      destination.update(update)
  }
  
  private func createTimer() {
      let _timer = DispatchSource.makeTimerSource(queue: queue)
      _timer.schedule(deadline: DispatchTime.now() + timerInterval)
      _timer.setEventHandler { self.timerHandler() }
      _timer.resume()
      
      timer.access { $0 = _timer }
  }
  
    private func timerHandler() {
        if let update = nextUpdate.read {
            sendNow(update: update)
            createTimer()
        } else {
            timer.access { $0 = nil }
        }
    }
}
