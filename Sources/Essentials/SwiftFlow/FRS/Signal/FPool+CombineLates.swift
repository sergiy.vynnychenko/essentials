//
//  File.swift
//  
//
//  Created by Sergiy Vynnychenko on 28.08.2024.
//

import Foundation


public extension FSPool {
    func combineLatest<T1,T2>(_ f1: Flow.Signal<T1>, _ f2: Flow.Signal<T2>, queue: DispatchQueue = .global()) -> Flow.Signal<(T1,T2)> {
        let _combine = SCombineLatest2(f1, f2)
        self.retain(_combine)
        return _combine.siganl
    }
    
    func combineLatest<T1,T2,T3>(_ f1: Flow.Signal<T1>, _ f2: Flow.Signal<T2>, _ f3: Flow.Signal<T3>, queue: DispatchQueue = .global()) -> Flow.Signal<(T1,T2,T3)> {
        let _combine = SCombineLatest3(f1, f2, f3)
        self.retain(_combine)
        return _combine.siganl
    }
}


fileprivate class SCombineLatest2<T1, T2> {
    let queue = DispatchQueue(label: "SCombine2.Serial")
    let siganl = Flow.Signal<(T1,T2)>(queue: .global())
    var update1 : T1?
    var update2 : T2?
    
    init(_ s1: Flow.Signal<T1>, _ s2: Flow.Signal<T2>) {
        s1.onUpdate(context: self, queue: queue) { ctx, upd in     ctx.update1 = upd;      ctx.trySendUpdate() }
        s2.onUpdate(context: self, queue: queue) { ctx, upd in     ctx.update2 = upd;      ctx.trySendUpdate() }
    }
    
    func trySendUpdate() {
        guard let upd1 = update1 else { return }
        guard let upd2 = update2 else { return }
        
        siganl.update((upd1,upd2))
    }
}


fileprivate class SCombineLatest3<T1, T2, T3> {
    let queue = DispatchQueue(label: "SCombine3.Serial")
    let siganl = Flow.Signal<(T1,T2,T3)>(queue: .global())
    var update1 : T1?
    var update2 : T2?
    var update3 : T3?
    
    init(_ s1: Flow.Signal<T1>, _ s2: Flow.Signal<T2>, _ s3: Flow.Signal<T3>) {
        s1.onUpdate(context: self, queue: queue) { ctx, upd in     ctx.update1 = upd;      ctx.trySendUpdate() }
        s2.onUpdate(context: self, queue: queue) { ctx, upd in     ctx.update2 = upd;      ctx.trySendUpdate() }
        s3.onUpdate(context: self, queue: queue) { ctx, upd in     ctx.update3 = upd;      ctx.trySendUpdate() }
    }
    
    func trySendUpdate() {
        guard let upd1 = update1 else { return }
        guard let upd2 = update2 else { return }
        guard let upd3 = update3 else { return }
        
        siganl.update((upd1,upd2,upd3))
    }
}
