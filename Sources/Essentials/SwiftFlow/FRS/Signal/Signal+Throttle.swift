//
//  File.swift
//  
//
//  Created by Sergiy Vynnychenko on 15.02.2025.
//

import Foundation

extension Flow {
    public enum AfterThrottling {
      case sendLast
      case sendFirst
      case none
    }
}

public extension Flow.Signal {
    func throttle(interval: Double, after: Flow.AfterThrottling = .sendLast) -> Flow.Signal<Update> {
        let helper = ThrottleEventSourceHelper(destination: Update.self, interval: interval, qos: .userInteractive, after: after)
        self.retain(helper)
        
        
        self.onUpdate(context: helper) { helper, update in
            helper.onUpdate(update: update)
        }
        
        return helper.destination
    }
}

private class ThrottleEventSourceHelper<Update> {
    let after: Flow.AfterThrottling
    var nextUpdate = LockedVar<Update?>(nil)
    let queue: DispatchQueue
    var timer = LockedVar<DispatchSourceTimer?>(nil)
    var timerInterval: DispatchTimeInterval
    var destination: Flow.Signal<Update>
    
    init(destination: Update.Type, interval: Double, qos: DispatchQoS.QoSClass, after: Flow.AfterThrottling) {
        
        self.destination = Flow.Signal(queue: .global(qos: qos))
        self.timerInterval = interval.dispatchInterval
        self.queue = DispatchQueue.init(label: "SwiftFlow.throttle Timer DispatchQueue", qos: .userInteractive)
        self.after = after
    }
    
//  func eventHandler(source: Source) -> AnyObject? {
//    return source.makeHandler(executor: Executor.queue(self.queue)) { (event, originalExecutor) in
//            
//      switch event {
//      case let .completion(completion):   self.onComplete(completion: completion, executor: originalExecutor)
//      case let .update(update):           self.onUpdate(update: update)
//      }
//    }
//  }
  
    func onUpdate(update: Update) {
        if timer.read == nil {
            sendNow(update: update)
            createTimer()
        } else {
            
            // decide which update to send
            // after throttling interval
            switch after {
            case .sendLast:     nextUpdate.access { $0 = update }
            case .sendFirst:    if nextUpdate.read == nil { nextUpdate.access { $0 = update } }
            case .none:         break
            }
            
        }
    }
}

extension ThrottleEventSourceHelper {
  private func sendNow(update: Update) {
      nextUpdate.access { $0 = nil }
      destination.update(update)
  }
  
  private func createTimer() {
      let _timer = DispatchSource.makeTimerSource(queue: queue)
      _timer.schedule(deadline: DispatchTime.now() + timerInterval)
      _timer.setEventHandler { self.timerHandler() }
      _timer.resume()
      
      timer.access { $0 = _timer }
  }
  
    private func timerHandler() {
        if let update = nextUpdate.read {
            sendNow(update: update)
            createTimer()
        } else {
            timer.access { $0 = nil }
        }
    }
}

internal extension Double {
  var dispatchInterval: DispatchTimeInterval {
    let microseconds = Int64(self * 1000000) // perhaps use nanoseconds, though would more often be > Int.max
    return microseconds < Int.max ? DispatchTimeInterval.microseconds(Int(microseconds)) : DispatchTimeInterval.seconds(Int(self))
  }
}
