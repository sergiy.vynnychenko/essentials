
import Foundation

public extension Flow.Signal {
    // Assign Success
    @discardableResult
    func assign<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Update>, queue : DispatchQueue = .main) -> Self {
        onUpdate(queue: queue) { [weak context] update in
            guard let context = context else { return }
            context[keyPath: keyPath] = update
        }
    }
    
    @discardableResult
    func assign<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, Update?>, queue : DispatchQueue = .main) -> Self {
        onUpdate(queue: queue) { [weak context] update in
            guard let context = context else { return }
            context[keyPath: keyPath] = update
        }
    }
    
    @discardableResult
    func append<Context:AnyObject>(on context: Context, to keyPath: ReferenceWritableKeyPath<Context, [Update]>, queue : DispatchQueue = .main) -> Self {
        onUpdate(queue: queue) { [weak context] update in
            guard let context = context else { return }
            context[keyPath: keyPath].append(update) 
        }
    }
    
    @discardableResult
    func onUpdate(queue: DispatchQueue? = nil, block: @escaping (Update)->Void) -> Self {
        let handler = Handler(queue: queue ?? self.queue, block: block)
        updateHandlers.access {
            $0.append(handler)
        }
        
        if let upd = update.read {
            handler.schedule(upd)
        }
        return self
    }
    
    @discardableResult
    func onUpdate<Context:AnyObject>(context: Context, queue: DispatchQueue? = nil, block: @escaping (Context,Update)->Void) -> Self {
        onUpdate(queue: queue ?? self.queue) { [weak context] update in
            guard let context = context else { return }
            block(context,update)
        }
    }
    
    @discardableResult
    func updating(signal another: Flow.Signal<Update>, queue : DispatchQueue? = nil, retain: Bool = false) -> Self {
        if retain {
            onUpdate(queue: queue) { success in
                another.update(success)
            }
        } else {
            onUpdate(queue: queue) { [weak another] success in
                another?.update(success)
            }
        }
        return self
    }
}
