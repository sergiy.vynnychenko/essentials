
import Foundation

public extension Flow.Signal {
    func filter(queue: DispatchQueue? = nil, block: @escaping (Update)->Bool) -> Flow.Signal<Update> {
        let nextSignal = Flow.Signal<Update>(queue: queue ?? self.queue)

        self.onUpdate(queue: queue ?? self.queue) { update in
            if block(update) {
                nextSignal.update(update)
            }
        }
        
        return nextSignal
    }
    
    func filter<Context:AnyObject>(context: Context, queue: DispatchQueue? = nil, block: @escaping (Context,Update)->Bool) -> Flow.Signal<Update> {
        return filter(queue: queue) { [weak context] update in
            guard let context = context else { return false }
            return block(context, update)
        }
    }
    
    func skip(first: Int, queue: DispatchQueue? = nil) -> Flow.Signal<Update> {
        let nextSignal = Flow.Signal<Update>(queue: queue ?? self.queue) // will be retained by the closure bellow
        var counter = first
        
        self.onUpdate(queue: queue ?? self.queue) { update in
            counter -= 1
            guard counter < 0 else { return }
            nextSignal.update(update)
        }

        return nextSignal
    }
}
