
import Foundation

public extension Flow.Signal {
    func map<NewUpdate>(queue : DispatchQueue? = nil, block: @escaping (Update) -> (NewUpdate)) -> Flow.Signal<NewUpdate> {
        let s = Flow.Signal<NewUpdate>(queue: queue ?? self.queue)
        self.onUpdate { update in s.update(block(update)) } // will retain instance of s
        return s
    }
    
    func map<Context, NewUpdate>(context: Context, queue : DispatchQueue? = nil, block: @escaping (Context,Update) -> (NewUpdate)) -> Flow.Signal<NewUpdate> {
        let s = Flow.Signal<NewUpdate>(queue: queue ?? self.queue)
        self.onUpdate { update in s.update(block(context,update)) } // will retain instance of s
        return s
    }
    
    func distinct() -> Flow.Signal<Update> where Update : Equatable {
        let s = Flow.Signal<Update>(queue: self.queue)
        self.onUpdate { update in // will retain instance of s
            if update != s.currentValue {
                s.update(update)
            }
        }
        return s
    }
    
    func skip(first: Int) -> Flow.Signal<Update> {
        let s = Flow.Signal<Update>(queue: self.queue)
        var numberOfFirstToSkip = LockedVar(first)
        
        self.onUpdate { update in // will retain instance of s
            if numberOfFirstToSkip.read <= 0 {
                s.update(update)
            } else {
                numberOfFirstToSkip.access { $0 -= 1 }
            }
        }
        return s
    }
    
    func skipFirst() -> Flow.Signal<Update> {
        skip(first: 1)
    }
}
