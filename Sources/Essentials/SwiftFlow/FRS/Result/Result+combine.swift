
public func combine<T1,T2>(_ r1: Result<T1,Error>, _ r2: Result<T2,Error>) -> Result<(T1,T2), Error> {
    return r1.combine(with: r2)
}

public func combine<T1,T2,T3>(_ r1: Result<T1,Error>, _ r2: Result<T2,Error>, _ r3: Result<T3,Error>) -> Result<(T1,T2,T3), Error> {
    return combine(r1, r2)
        .combine(with: r3)
        .map { return ($0.0, $0.1, $1) }
}

public func combine<T1,T2,T3,T4>(_ r1: Result<T1,Error>, _ r2: Result<T2,Error>, _ r3: Result<T3,Error>, _ r4: Result<T4,Error>) -> Result<(T1,T2,T3,T4), Error> {
    return combine(r1, r2, r3)
        .combine(with: r4)
        .map { return ($0.0, $0.1, $0.2, $1) }
}

extension Result {
    func combine<T2>(with other: Result<T2, Error>) -> Result<(Success,T2), Error> {
        switch self {
        case .success(let selfRes):
            switch other {
            case .success(let otherRes):
                return .success( (selfRes, otherRes) )
            case .failure(let error):
                return .failure(error)
            }
        case .failure(let error):
            return .failure(error)
        }
    }
}
