
public protocol ResultIterator {
    associatedtype Success
    
    func next() -> Result<Success?, Error>
}

public extension ResultIterator {
    func all() -> Result<[Success],Error> {
        var c = [Success]()
        
        var result = next()
        
        while c.insert(next: result) {
          result = next()
        }
        
        if let error = result.maybeFailure {
            return .failure(error)
        }
        
        return .success(c)
    }
}

extension Array {
    mutating func insert(next: Result<Element?, Error>) -> Bool {
        switch next {
        case let .success(item):
            if let item = item {
                self.append(item)
                return true
            }
            return false
        default:
            return false
        }
    }
}
