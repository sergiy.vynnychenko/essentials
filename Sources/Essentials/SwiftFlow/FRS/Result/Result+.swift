
import Foundation

public typealias Fallible<S> = Result<S, Swift.Error>
public typealias R<S> = Result<S, Swift.Error>

public extension Result {
    @discardableResult
    func dump() -> Self {
        switch self {
        case .success(let success):
            print("content: \(success)")
        case .failure(let error):
            print("ERROR: \(error.localizedDescription)")
        }
        return self
    }
    
    @discardableResult
    func onSuccess(_ block: (Success)->Void) -> Self {
        switch self {
        case .success(let success): block(success)
        default: break
        }
        return self
    }
    
    @discardableResult
    func onFailure(_ block: (Error)->Void) -> Self {
        switch self {
        case .failure(let error): block(error)
        default: break
        }
        return self
    }
    
    @discardableResult
    func onSuccess(_ block: (Success) async -> Void) async -> Self {
        switch self {
        case .success(let success): await block(success)
        default: break
        }
        return self
    }
    
    @discardableResult
    func onFailure(_ block: (Error) async -> Void) async -> Self {
        switch self {
        case .failure(let error): await block(error)
        default: break
        }
        return self
    }
    
    var maybeSuccess: Success? {
        switch self {
        case let .success(s):
            return s
        default:
            return nil
        }
    }
    
    var maybeFailure: Failure? {
        switch self {
        case let .failure(error):
            return error
        default:
            return nil
        }
    }
}

public extension Result {
    @available(*, deprecated, message: "use .if instead")
    func flatMap<NewSuccess>(if   if_  : (Success)->Bool,
                             then then_: (Success)->Result<NewSuccess,Failure>,
                             else else_: (Success)->Result<NewSuccess,Failure>) -> Result<NewSuccess,Failure> {
        self.flatMap {
            if if_($0) {
                return then_($0)
            } else {
                return else_($0)
            }
        }
    }
    
    typealias ResultCondition = (Success)->Result<Success,Failure>
    
    func `if`(_ cond: Bool, then then_: ResultCondition) -> Result<Success,Failure> {
        self.flatMap {
            if cond {
                return then_($0)
            } else {
                return .success($0)
            }
        }
    }
    
    func `if`(_ cond: (Success)->Bool, then then_: ResultCondition) -> Result<Success,Failure> {
        self.flatMap { self.if(cond($0), then: then_) }
    }
    
    func `if`(_ keyPath : KeyPath<Success, Bool>, then then_: ResultCondition, else else_: ResultCondition) -> Result<Success,Failure> {
        self.flatMap { self.if($0[keyPath: keyPath], then: then_) }
    }
    
    
    func `if`<NewSuccess>(_ cond    : Bool,
                          then then_: (Success)->Result<NewSuccess,Failure>,
                          else else_: (Success)->Result<NewSuccess,Failure>) -> Result<NewSuccess,Failure> {
        self.flatMap {
            if cond {
                return then_($0)
            } else {
                return else_($0)
            }
        }
    }
    
    func `if`<NewSuccess>(_ cond    : (Success)->Bool,
                          then then_: (Success)->Result<NewSuccess,Failure>,
                          else else_: (Success)->Result<NewSuccess,Failure>) -> Result<NewSuccess,Failure> {
        
        self.flatMap { self.if(cond($0), then: then_, else: else_) }
    }
    
    func `if`<NewSuccess>(_ keyPath : KeyPath<Success, Bool>,
                          then then_: (Success)->Result<NewSuccess,Failure>,
                          else else_: (Success)->Result<NewSuccess,Failure>) -> Result<NewSuccess,Failure> {
        
        self.flatMap { self.if($0[keyPath: keyPath], then: then_, else: else_) }
    }
    
    func ifError(code: Int, block: ()->Result<Success,Failure>) -> Result<Success,Failure> {
        flatMapError { error in
            let nsError = error as NSError
            
            if nsError.code == code {
                return block()
            } else {
                return .failure(error)
            }
        }
    }
}

public extension String {
    static func validatingUTF8(cString: UnsafePointer<CChar>) -> Result<String, Error> {
        if let str = String(validatingUTF8: cString) {
            return .success(str)
        } else {
            return .failure(WTF("can't validate cString"))
        }
    }
}

prefix operator ⌘
public prefix func ⌘<T>(right: T) -> Result<T, Error> {
    return .success(right)
}

public extension Result {
    static func |<Transformed>(left: Self, right: (Success)->Transformed) -> Result<Transformed, Failure> { // 2
        return left.map { right($0) }
    }
    
    static func |<Transformed>(left: Self, right: (Success)->Result<Transformed, Failure>) -> Result<Transformed, Failure> { // 2
        return left.flatMap { right($0) }
    }
}

public extension Result {
    @discardableResult
    func assignRError<T>(to keyPath: ReferenceWritableKeyPath<T, Error?>, on context: T) -> Self {
        switch self {
            case .failure( let error):
                context[keyPath: keyPath] = error
            default: break
        }
        
        return self
    }
    
    @discardableResult
    func assignRSuccess<T>(to keyPath: ReferenceWritableKeyPath<T, Success>, on context: T) -> Self {
        switch self {
            case .success( let success):
                context[keyPath: keyPath] = success
            default: break
        }
        
        return self
    }
}

public extension Result where Failure == Error {
    var asVoid : R<Void> {
        self.map { _ in () }
    }
}

public func result<T>(from t: T ) -> R<T> where T: Any {
    return .success(t)
}

public extension Result {
    func flatMapTry<NewSuccess>(catching body: (Success) throws -> NewSuccess) -> Result<NewSuccess, any Error>
        where Failure == Swift.Error
    {
        return self.flatMap { args in
            Swift.Result{ try body(args) }
        }
    }
}
