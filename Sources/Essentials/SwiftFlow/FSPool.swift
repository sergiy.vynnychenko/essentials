
import Foundation

public protocol FRetainer {
    func retain(_ obj: Any)
}



public class FSPool: FRetainer, ErrorFlow {
    public let errorPool = ErrorPool()
    var dic       = LockedVar<[FutureID:FutureRef]>([:])
    var container = LockedVar<[Any]>([])
    public let queue : DispatchQueue
    
    public var hasRecentError : Bool { errorPool.recentError.currentValue != nil }
    
    public init(queue: DispatchQueue) {
        self.queue = queue
    }
    
    public func retain(_ obj: Any) {
        container.access {
            $0.append(obj)
        }
    }
    
    @discardableResult
    public func retain<T>(_ f: Flow.Future<T>) -> Flow.Future<T> {
        dic[f.id] = FutureRef(ref: f)
        return f
    }
    
    public func append(error: Error) {
        errorPool.append(error: error)
    }
    public func forwardErrors(to errorFlow: ErrorFlow) {
        errorPool.forwardErrors(to: errorFlow)
    }
}

public class FutureRef {
    var ref : Any?
    init(ref: Any) {
        self.ref = ref
    }
}


public extension Result where Failure == Error {
    
    @discardableResult
    func failing(into flow: ErrorFlow) -> Self {
        self.onFailure { error in flow.append(error: error) }
        return self
    }
    
    @discardableResult
    func failing(into flow: ErrorFlow?) -> Self {
        self.onFailure { error in flow?.append(error: error) }
        return self
    }
}
