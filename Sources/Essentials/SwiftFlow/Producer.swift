//
//  File.swift
//  
//
//  Created by Sergiy Vynnychenko on 27.08.2024.
//

import Foundation

public extension Flow {
    class Producer<T> {
        public let pool         : FSPool
        public let signal       : Signal<T>
        public let inProgress   : Signal<Int> //initial value should be 0
        
        private var _inProgressCount = LockedVar<Int>(0)
        
        public init(poolID: FSPoolID) {
            self.pool = poolID.pool
            self.signal = Signal<T>(queue: pool.queue)
            self.inProgress = Signal<Int>(queue: pool.queue, update: 0)
        }
        
        public init(poolID: FSPoolID, block : @escaping ()->R<T>) {
            self.pool = poolID.pool
            self.signal = Signal<T>(queue: pool.queue)
            self.inProgress = Signal<Int>(queue: pool.queue, update: 0)
        }
        
        @discardableResult
        public func refresh<Context:AnyObject>(context: Context, block: @escaping (Context)->R<T>) -> Future<T> {
            refresh { [weak context] in
                guard let c = context else { return .wtf("context is nil") }
                return block(c)
            }
        }
        
        @discardableResult
        public func refresh(block: @escaping ()->R<T>) -> Future<T> {
            inc(1)
            
            return pool.future(block: block)
                .onSuccess(context: self) { me, success in
                    me.signal.update(success)
                }
                .onFailure(context: self) { me, error in
                    me.pool.append(error: error)
                }
                .onComplete(context: self) { me, _ in
                    me.inc(-1)
                }
        }
        
        func inc(_ value: Int) {
            let current = _inProgressCount.access { $0 += value; return $0 }
            inProgress.update(current)
        }
    }
}
