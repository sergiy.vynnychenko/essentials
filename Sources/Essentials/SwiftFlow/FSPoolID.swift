
import Foundation

fileprivate var pools = LockedVar<[FSPoolID:FSPool]>([:])

public struct FSPoolID : Hashable, Codable {
    public let id : String
    
    public init(id: String) {
        self.id = id
    }
}

public extension FSPoolID {
    var pool : FSPool {
        if let p = pools[self] {
            return p
        }
        let p = FSPool(queue: .global())
        pools[self] = p
        return p
    }
}
