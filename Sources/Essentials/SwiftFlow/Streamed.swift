
import Foundation

@propertyWrapper public struct Streamed<Value> {
    let initialValue : Value
    public var wrappedValue : Value { get { _signal.currentValue ?? initialValue  } set { _signal.update(newValue) } }
    
    /// The property that can be accessed with the `$` syntax and allows access to the `Channel`
    public var projectedValue: Flow.Signal<Value> { get { return _signal } }
    
    private let _signal : Flow.Signal<Value>
    
    /// Initialize the storage of the Published property as well as the corresponding `Publisher`.
    public init(wrappedValue: Value, queue: DispatchQueue = .global(qos: .userInteractive)) {
        self.initialValue = wrappedValue
        self._signal = Flow.Signal(queue: queue, update: wrappedValue)
    }
}
