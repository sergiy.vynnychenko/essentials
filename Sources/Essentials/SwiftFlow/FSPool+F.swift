
import Foundation

public extension FSPool {
    func discard(id: FutureID) {
        dic[id] = nil
    }
    
    func promise<T>(block: @escaping (Flow.Future<T>)->Void) -> Flow.Future<T> {
        Flow.Future(queue: queue, block: block)
            .retain(at: self)
    }
    
    func future<T>(block: @escaping ()->R<T>) -> Flow.Future<T> {
        Flow.Future(queue: queue, block: block)
            .retain(at: self)
    }
    
    func future<T>(_ operation: @Sendable @escaping () async throws -> T) -> Flow.Future<T> {
        Flow.Future(queue: queue, operation)
            .retain(at: self)
    }
    
    func future<T>(_ operation: @Sendable @escaping () async -> T) -> Flow.Future<T> {
        Flow.Future(queue: queue, operation)
            .retain(at: self)
    }
}

extension Flow.Signal {
    @discardableResult
    func runFailing<T>(on pool: FSPool, block: @escaping (Update)->R<T>) -> Self {
        onUpdate { [weak pool] update in
            _ = pool?.runFailing { block(update) }
        }
    }
}

extension FSPool {
    func runFailing<T>(block: @escaping ()->R<T>) -> Flow.Future<T> {
        future(block: block).failing(into: self)
    }
}
