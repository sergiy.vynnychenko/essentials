
import Foundation

private let shortThreshold : Int = 100

public class ErrorsViewModel : ObservableObject {
    let pool : ErrorPool
    @Published var recentError : Error?
    
    var teaser : String { recentError?.localizedDescription.first(shortThreshold) ?? "" }
    var isShort : Bool { recentError?.isShort ?? false }
    var countTextLines : Int { recentError?.countTextLines ?? 0 }
    
    init(pool: ErrorPool) {
        self.pool = pool
        
        self.recentError = pool.recentError.currentValue
        
        pool.recentError
            .skip(first: 1)
            .assign(on: self, to: \.recentError)
    }
    
    func clearRecentError() {
        pool.clearRecent()
        recentError = nil
    }
}

extension Error {
    var isShort : Bool {
        localizedDescription.count < shortThreshold
    }
    
    var countTextLines : Int {
        localizedDescription.split(bySeparators: ["\n"]).count
    }
}
