
import Foundation
import SwiftUI

public protocol ErrorFlow : AnyObject {
    func append(error: Error)
    func forwardErrors(to: ErrorFlow)
}

public class ErrorPool : ErrorFlow {
    private(set) var errors      = LockedVar<[Error]>([])
    public       let recentError = Flow.Signal<Error>(queue: .main)
    
    public func clearRecent() {
        recentError.clearBuffer()
    }
    
    public func append(error: Error) {
        self.errors.access { $0.append(error) }
        recentError.update(error)
    }
    
    public func forwardErrors(to errorFlow: ErrorFlow) {
        recentError.onUpdate{ [weak errorFlow] error in errorFlow?.append(error: error)}
    }
}

