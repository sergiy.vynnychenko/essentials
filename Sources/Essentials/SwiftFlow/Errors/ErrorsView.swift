
import Foundation
import SwiftUI
#if canImport(AppKit)
import AppKit
#endif

#if os(macOS)

@available(macOS 12.0, *)
public extension FSPool {
    @ViewBuilder
    var errorView : some View {
        ErrorsView(model: ErrorsViewModel(pool: self.errorPool))
    }
}

@available(macOS 12.0, *)
extension NSPasteboard {
    static func set(text: String) {
        general.clearContents()
        general.setString(text, forType: .string)
    }
}

@available(macOS 12.0, *)
public struct ErrorsView : View {
    @ObservedObject var model : ErrorsViewModel
    @State var isPresented: Bool = false
    
    public var body: some View {
        if let error = model.recentError {
            HStack {
                Button(action: { isPresented.toggle() }) {
                    Text("Error: ")
                        .foregroundColor(.red)
                        .bold()
                    
                    if model.isShort {
                        Text(error.localizedDescription)
                    } else {
                        Text(model.teaser + "...")
                    }
                    
                    Text("^").fontWeight(.ultraLight)
                }
                .buttonStyle(PlainButtonStyle())
                .popover(isPresented: $isPresented) {
                    ScrollView {
                        Button(action: { NSPasteboard.set(text: error.detailedDescription_MultiLine)  }) {
                            Text(error.detailedDescription_MultiLine)
                                .padding()
                        }.buttonStyle(PlainButtonStyle())
                    }
                    .frame(maxWidth: 900, maxHeight: 500)
                }
                
                
                Button(action: { model.clearRecentError() }) {
                    Text("Dismiss")
                }
            }
            .help(error.localizedDescription)
            .padding()
            .background {
                RoundedRectangle(cornerRadius: 5)
                    .stroke()
                    .foregroundColor(.red)
            }
            
        } else {
            EmptyView()
        }
    }
}

#endif
