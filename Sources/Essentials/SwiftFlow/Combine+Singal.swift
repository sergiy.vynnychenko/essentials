
import Foundation
import Combine

@available(OSX 10.15, *)
public extension Published.Publisher {
    func flow(in pool: FSPool) -> Flow.Signal<Value> {
        let s = Flow.Signal<Value>(queue: .global(qos: .userInteractive))
        
        let sink = self.sink { value in
            s.update(value)
        }
        
        pool.retain(sink)
        
        return s
    }
}
