import Foundation
import SwiftUI

@available(iOS 13.4, *)
public extension Flow {
    static func jsonFileIO<Content:Codable>(url: URL) -> FileIO<Content> {
        FileIO(url: url,
               read: { url in url.readToString.flatMap{ $0.decodeFromJson(type: Content.self) } },
               write: { url, content in content.asJson().flatMap{ json in url.write(content: json) }.asVoid })
    }
    
    struct FileIO<Content> {
        public let url: URL
        public let read : (URL) -> R<Content>
        public let write : (URL, Content) -> R<Void>
        
        public init(url: URL, read: @escaping (URL) -> R<Content>, write: @escaping (URL, Content) -> R<Void>) {
            self.url = url
            self.read = read
            self.write = write
        }
        
        public func readContent() -> R<Content> { read(url) }
        public func write(content: Content) -> R<Void> { write(url, content) }
    }
}

@available(iOS 13.4, *)
public extension Flow.Document {
    class RefreshViewModel : ObservableObject {
        public init(doc: Flow.Document<Content>) {
            doc.$content
                .skipFirst()
                .onUpdate(context: self, queue: .main) { me, _ in
                    me.objectWillChange.send()
                }
        }
    }
}

@available(iOS 13.4, *)
public extension Flow {
    class Document<Content> {
        public let pool : FSPool
        let io : FileIO<Content>
        public var url : URL { io.url }
        
        public var refreshVM : RefreshViewModel { .init(doc: self) }
        
        @Streamed public var content : Content
        
        public init(url: URL, defaultContent: Content, errors: ErrorFlow? = nil) where Content : Codable {
            let _io : FileIO<Content> = Flow.jsonFileIO(url: url)
            let serialQueue = DispatchQueue(label: _io.url.path)
            let initialContent = _io.readContent().maybeSuccess ?? defaultContent
            
            self.pool = FSPool(queue: serialQueue)
            self.io = _io
            self._content = Streamed(wrappedValue: initialContent, queue: serialQueue)
         
            if let errors {
                pool.forwardErrors(to: errors)
            }
            subscribe()
        }
        
        public init(io: FileIO<Content>, defaultContent: Content, errors: ErrorFlow? = nil) {
            let serialQueue = DispatchQueue(label: io.url.path)
            
            // Use the default content if reading from the file fails
            let initialContent = io.readContent().maybeSuccess ?? defaultContent
            
            self.pool = FSPool(queue: serialQueue)
            self.io = io
            self._content = Streamed(wrappedValue: initialContent, queue: serialQueue)
            
            if let errors {
                pool.forwardErrors(to: errors)
            }
            subscribe()
        }
        
        func subscribe() {
            let _io = self.io
            $content
                .throttle(interval: 1)
                // Write on pool's queue and use pool's error stream
                .runFailing(on: pool) { content in _io.write(content: content) }
        }
    }
}
