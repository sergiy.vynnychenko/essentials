
import Foundation
import SwiftUI

extension Flow {
    public struct MemDataIO<Content> {
        public let read : () -> R<Content>
        public let write : (Content) -> R<Void>
        
        public init( read: @escaping () -> R<Content>, write: @escaping (Content) -> R<Void>) {
            self.read = read
            self.write = write
        }
        
        public func readContent() -> R<Content> { read() }
        public func write(content: Content) -> R<Void> { write(content) }
    }
}

public extension Flow.MemData {
    class RefreshViewModel : ObservableObject {
        public init(data: Flow.MemData<Content>) {
            data.$content
                .skipFirst()
                .onUpdate(context: self, queue: .main) { me, _ in
                    me.objectWillChange.send()
                }
        }
    }
}

public extension Flow {
    class MemData<Content> {
        public let pool : FSPool
        public var id : UUID
        let io : MemDataIO<Content>
        
        public var newRefreshVM : RefreshViewModel { .init(data: self) }
        
        @Streamed public var content : Content
        
        public init(io: MemDataIO<Content>, defaultContent: Content, errors: ErrorFlow? = nil) {
            self.id = UUID()
            let serialQueue = DispatchQueue(label: "Flow.MemData.\(Content.self).\(id.uuidString)")
            
            // Use the default content if reading from the file fails
            let initialContent = io.readContent().maybeSuccess ?? defaultContent
            
            self.pool = FSPool(queue: serialQueue)
            self.io = io
            self._content = Streamed(wrappedValue: initialContent, queue: serialQueue)
            
            if let errors {
                pool.forwardErrors(to: errors)
            }
            
            subscribe()
        }
        
        func subscribe() {
            let _io = self.io
            $content
                .throttle(interval: 1)
                // Write on pool's queue and use pool's error stream
                .runFailing(on: pool) { content in _io.write(content: content) }
        }
    }
}
