//
//  File.swift
//  
//
//  Created by Sergiy Vynnychenko on 28.08.2024.
//

import Foundation

fileprivate var queues = LockedVar<[DispatchQueueID:DispatchQueue]>([:])

public struct DispatchQueueID : Hashable {
    let id: String
}

public extension DispatchQueueID {
    var queue : DispatchQueue {
        if let q = queues[self] {
            return q
        }
        let q = DispatchQueue(label: self.id, qos: .userInitiated)
        queues[self] = q
        return q
    }
}

