@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public extension Result {
    func wait() async -> Self {
        return self
    }
}

@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public actor AsyncFunc<T> {
    let task : Task<T, Error>
    public init(block: @escaping () -> Result<T,Error>) {
        self.task = Task {
            try await block().wait().get()
        }
    }
}

@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public func asyncFunc<T>(block: @escaping () -> Result<T,Error>) async -> Result<T,Error> {
    let af = AsyncFunc { block() }
    return await af.task.result
}
