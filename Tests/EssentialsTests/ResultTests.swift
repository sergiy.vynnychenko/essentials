import XCTest
@testable import Essentials

func div(a: Double, b: Double) -> Result<Double, Error> {
    if b == 0 {
        return .failure(WTF("division by zero"))
    } else {
        return .success(a/b)
    }
}

extension String {
    func asInt() -> Result<Int, Error> {
        if let int = Int(self) {
            return .success(int)
        } else {
            return .failure(WTF("can't convert to int: \(self)"))
        }
    }

}

class ResultTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReduce() throws {
        [1.0, 2.0, 2.0]
            .reduce(0) { div(a: $0, b: $1) }
            .onSuccess { XCTAssert( ($0 - 0.25) < 0.01) }
            .onFailure { _ in XCTAssert(false) }
        
        [1.0, 2.0, 0.0]
            .reduce(0) { div(a: $0, b: $1) }
            .onSuccess { _ in XCTAssert(false) }
            .onFailure {
                print($0.localizedDescription);
                //XCTAssert($0.localizedDescription == "division by zero")
            }
        
        let r = [1.0, 2.0, 0.0]
            .reduceCatch(0) { div(a: $0, b: $1) }

        XCTAssert( (r.results - 0.6) < 0.01)
        XCTAssert( (r.errors.count == 1))
        print(r.errors);
        
    }
    
    func testFlatMap() throws {
        ["1","2","3"]
            .flatMap { $0.asInt() }
            .onSuccess { XCTAssert( $0.elementsEqual([1,2,3])) }
        
        ["1","2","3", ""]
            .flatMap { $0.asInt() }
            .onSuccess { _ in XCTAssert(false) }
            .dump()
        
        let r = ["1","2","3", ""]
            .flatMapCatch { $0.asInt() }

        XCTAssert( r.results.elementsEqual([1,2,3]))
        XCTAssert( (r.errors.count == 1))
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}

