import XCTest
@testable import Essentials

final class ArrayTests: XCTestCase {
    func testConcatenation() {
      XCTAssert([1,2,3,4].elementsEqual([1,2,3] + 4))
      
      var arr = [1,2,3]
      XCTAssert([1,2,3,4].elementsEqual(arr += 4))
    }
    
    func testChanked() {
        let chBy2 = [1,2,3,4,5,6]
            .chunked(by: 2)
        
        XCTAssert( chBy2.elementsEqual([[1,2],[3,4],[5,6]]))
        
        let chInto2 = [1,2,3,4,5,6]
            .chunked(into: 2)
        
        XCTAssert( chInto2.elementsEqual([[1,2,3,],[4,5,6]]))
    }

    static var allTests = [
        ("testConcatenation", testConcatenation),
    ]
}
