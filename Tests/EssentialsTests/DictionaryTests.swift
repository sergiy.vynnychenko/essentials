import XCTest

class DictionaryTests: XCTestCase {
    func testDiff() {
        let AaBbCc = ["A":"a", "B":"b", "C":"c"]
        let AaCcDd = ["A":"a", "C":"c", "D":"d"]
        let AABBCC = ["A":"A", "B":"B", "C":"C"]
        
        XCTAssert(AaBbCc.diff(with: AaBbCc) == [])
        XCTAssert(AaBbCc.diff(with: AaCcDd) == ["B", "D"])
        XCTAssert(AaBbCc.diff(with: AABBCC) == ["A","B","C"])
    }
}
