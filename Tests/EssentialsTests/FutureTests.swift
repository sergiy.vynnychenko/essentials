
import Foundation
import XCTest
import Essentials

final class FutureTests: XCTestCase {

    
    func testFuture() {
        let sema = DispatchSemaphore(value: 0)
        
        let pool = FSPool(queue: .global())
        
        pool.future {
            return .success("hello")
        }
        .onSuccess(queue: .global()) { str in
            print(str + " [" + Thread.current.dbgName + "] " )
            sema.signal()
        }
        
        sema.wait()
        
        print("finish")
    }
    
    func testPromise() {
        let sema = DispatchSemaphore(value: 0)
        
        let pool = FSPool(queue: .global())
        
        pool.promise { promise in
            promise.complete(.success("hello"))
        }
        .onSuccess(queue: .global()) { str in
            print(str + " [" + Thread.current.dbgName + "] " )
            sema.signal()
        }
        
        sema.wait()
        
        print("finish")
    }
    
    func testWait() {
        
        let pool = FSPool(queue: .global())
        
        pool.promise { promise in
            print()
            sleep(1)
            promise.complete(.success("hello"))
        }
        .wait()
        .onSuccess { str in print("success: " + str) }
        
        print("finish")
    }
    
    func testFutureInsideFuture() {
        let pool = FSPool(queue: .global())
        
        let f1 = pool.future { .success("success_f1") }
        let f2 = pool.future { f1.wait() }
        
        f2.wait()
            .shouldSucceed("f2.wait")
        
        print("finish")
    }
    
    func testSkip() {
        let sema = DispatchSemaphore(value: 0)
        let s = Signal<Int>(queue: .init(label: "serial"))
        
        s.skip(first: 1)
            .onUpdate { value in
                XCTAssert(value != 1)
                sema.signal()
            }
        
        s.update(1)
        s.update(2)
        
        sema.wait()
    }
}
