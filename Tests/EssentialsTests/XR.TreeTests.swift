import XCTest
@testable import Essentials

class XRTreeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testTree() {
        var tree = XR.Tree()
        let parent_1 = "SwiftGit2"
        let children_1 = ["SwiftGit2/Nimble",
                          "SwiftGit2/Quick"]
//                          "SwiftGit2_6/ZipArchive",
//                          "SwiftGit2_6/xcconfigs",
//                          "SwiftGit2_6/libssh2",
//                          "SwiftGit2_6/openssl"]
        
        


        let diff_1_1 = tree.update(parent: parent_1, children: children_1)
        print(diff_1_1)
        XCTAssert(diff_1_1.inserted.count == 3)
        
        for ch in children_1 {
            XCTAssert(tree.parentOf[ch] == parent_1)
            
        }
        XCTAssert(tree.childrenOf[parent_1]?.count == 2)
        
        let diff_1_2 = tree.update(parent: parent_1, children: children_1)
        print(diff_1_2)
        XCTAssert(diff_1_2.inserted.count == 0)
        
        let parent_2 = "SwiftGit2/Quick"
        let children_2 = ["SwiftGit2/Quick/Nimble"]
        
        let diff_2 = tree.update(parent: parent_2, children: children_2)
        print(diff_2)
        XCTAssert(diff_2.inserted.count == 1)
        
        let parent_3 = "SwiftGit2"
        let children_3 = ["SwiftGit2/Nimble",
                          "SwiftGit2/Quick",
                          "SwiftGit2_6/ZipArchive",
                          "SwiftGit2_6/xcconfigs",
                          "SwiftGit2_6/libssh2",
                          "SwiftGit2_6/openssl"]

        let diff_3 = tree.update(parent: parent_3, children: children_3)
        print(diff_3)
        XCTAssert(diff_3.inserted.count == 4)

    }
}
