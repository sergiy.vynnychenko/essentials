import XCTest
import Essentials

final class StringTruncTests: XCTestCase {
    func test_ShouldNotTruncate() {
        XCTAssert("12345".truncStart(length: 10) == "12345")
        
        XCTAssert("12345".truncEnd(length: 10) == "12345")
    }
    
    func test_ShouldTruncate() {
        let a = "12345678901234567890_1234567890".truncStart(length: 10)
        XCTAssert(a == "…1234567890")
        
        let b = "1234567890_12345678901234567890".truncEnd(length: 10)
        XCTAssert(b == "1234567890…")
        
        let c = "12345_67890123456789012345_67890".truncCenter(length: 10)
        XCTAssert(c == "12345…67890")
    }
    
    func test_IntIdxAccess() {
        let nums = "01234567890"
        XCTAssertEqual(nums[i: 1], "1")
        XCTAssertEqual(nums[i: 15], nil)
        
        //////////////////////
        // String
        //////////////////////
        ///
        let c1 = "/Users/uks/Documents"[i: 1]!
        XCTAssertEqual(c1, "U")
        
        let c2 = "a"[i: 0]
        XCTAssertEqual(c2, "a")
        
        let c3 = "a"[i: 1]
        XCTAssertEqual(c3, nil)
        
        let c4 = "a"[i: 50]
        XCTAssertEqual(c4, nil)
        
        //////////////////////
        // Substring
        //////////////////////
        let a = "/Users/uks/Documents".dropLast(1)[i: 1]!
        XCTAssertEqual(a, "U")
        
        let b = "as".dropLast(1)[0]
        XCTAssertEqual(b, "a")
        
        let c = "as".dropLast(1)[i:1]
        XCTAssertEqual(c, nil)
        
        let d = "as".dropLast(1)[i:50]
        XCTAssertEqual(d, nil)
    }
    
    func test_IntRangeIdxAccess() {
        let nums = "01234567890"
        
        XCTAssertEqual(nums[safe: -100 ... -1].asStr(), "")
        XCTAssertEqual(nums[safe: -1...20].asStr(), "01234567890")
        XCTAssertEqual(nums[safe: 11...20].asStr(), "")
        XCTAssertEqual(nums[safe: 0...5].asStr(), "012345")
        XCTAssertEqual(nums[safe: 0...5].asStr(), "012345")
        XCTAssertEqual(nums[safe: 1...5].asStr(), "12345")
        XCTAssertEqual(nums[safe: -1...5].asStr(), "012345")
        
        XCTAssertEqual(nums[safe: -100 ..< -1].asStr(), "")
        XCTAssertEqual(nums[safe: -1..<5].asStr(), "01234")
        XCTAssertEqual(nums[safe: 1..<5].asStr(), "1234")
        XCTAssertEqual("012"[safe: 1..<5].asStr(), "12")
        XCTAssertEqual("012"[safe: 1...5].asStr(), "12")
        XCTAssertEqual("012"[safe: -1...5].asStr(), "012")
        
        XCTAssertEqual(nums[safe: -100 ... -1].asStr(), "")
        
//        XCTAssertEqual(nums[unsafe: -100 ... -1].asString(), "") // will throw fatal error
//        XCTAssertEqual(nums[unsafe: -100 ..< -1].asString(), "") // will throw fatal error
    }
    
    func test_getSubstring() {
        let a =  "/Users/uks/Documents".getSubstring(location: 7, length: 3)
        XCTAssertEqual(a!, "uks")
        
        let b = "/Users/uks/Documents".getSubstring(location: -10, length: -3)
        XCTAssertEqual(b, nil)
        
        let c = "/Users/uks/Documents".getSubstring(location: 0, length: 50)
        XCTAssertEqual(c!, "/Users/uks/Documents")
        
        let d = "/Users/uks/Documents".getSubstring(location: 50, length: 50)
        XCTAssertEqual(d, nil)
    }
    
    func test_regex() {
        let regexResult = "a/s/d/f/".regex(pattern: ".+?[/]")
        
        XCTAssertEqual(regexResult[0], "a/")
        XCTAssertEqual(regexResult[3], "f/")
        
        let regexResult2 = "a/s/d/f/".regex(pattern: "asdfasdfasdf")
        XCTAssertEqual(regexResult2.count, 0)
    }
    
    func test_DeletingLastPathComponent() {
        //////////////////////
        //String
        //////////////////////
        let f = "/Users/uks/Documents".deletingLastPathComponent()
        XCTAssertEqual(f, "/Users/uks")
        
        let g = "UsersuksDocuments".deletingLastPathComponent()
        XCTAssertEqual(g, nil)
        
        let h = "/UsersuksDocuments".deletingLastPathComponent()
        XCTAssertEqual(h, nil)
        
        let i = "/U/sersuksDocuments".deletingLastPathComponent()
        XCTAssertEqual(i, "/U")
        
        let j = "/Users/uks/Documents/".deletingLastPathComponent()
        XCTAssertEqual(j, "/Users/uks")
        
        //////////////////////
        // Substring
        //////////////////////
        let a = "/Users/uks/Documentss".dropLast().deletingLastPathComponent()
        XCTAssertEqual(a, "/Users/uks")
        
        let b = "UsersuksDocumentss".dropLast().deletingLastPathComponent()
        XCTAssertEqual(b, nil)
        
        let c = "/UsersuksDocumentss".dropLast().deletingLastPathComponent()
        XCTAssertEqual(c, nil)
        
        let d = "/U/sersuksDocumentss".dropLast().deletingLastPathComponent()
        XCTAssertEqual(d, "/U")
        
        let e = "/Users/uks/Documents/s".dropLast().deletingLastPathComponent()
        XCTAssertEqual(e, "/Users/uks")
    }
    
    func test_SplitByArrayStrings() {
        let test = "asdf|fdsaPOP|Arn"
        
        var rez = test.split(bySeparators: ["POP","|"])
        
        XCTAssertEqual(["asdf","fdsa","","Arn"], rez)
        
        rez = test.split(bySeparators: ["|","POP"])
        
        XCTAssertEqual(["asdf","fdsa","","Arn"], rez)
    }
}
