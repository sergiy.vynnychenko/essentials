import XCTest
import CryptoKit
//import EssentialsTesting
@testable import Essentials

class CryptoUksTests: XCTestCase {
    func test1_key() {
        let key = SymmetricKey(size: .bits256)
        
        let data = "Hello, Crypto!".data(using: .utf8)!
        
        let encrypted = data.encrypted(key: key).shouldSucceed()!
        let decryptedCorrect = encrypted.decrypted(key: key).shouldSucceed()
        let decryptedWrong = encrypted.decrypted(key: SymmetricKey(size: .bits256)).shouldFail()
    }
    
    func test2_pass_hash1() {
        let key1 = SymmetricKey(password: "12345").base64
        let key2 = SymmetricKey(password: "12345").base64
        
        XCTAssertEqual(key1, key2)
    }
    
    func test2_pass_hash2() {
        let key1Sha = SymmetricKey(size: .bits256).base64
        let key2Sha = SymmetricKey(base64Encoded: key1Sha)!.base64
        
        XCTAssertEqual(key1Sha, key2Sha)
    }
    
    func test3_pass_success() {
        let data = "Hello, Crypto!".data(using: .utf8)!
        
        let encrypted0 = data.encrypted(key: SymmetricKey(password: "12345")).shouldSucceed()!
        let encrypted1 = data.encrypted(password: "12345").shouldSucceed()!
        
        let decryptedCorrect1 = encrypted0.decrypted(password: "12345").shouldSucceed()!
        let decryptedCorrect2 = encrypted1.decrypted(password: "12345").shouldSucceed()!
        
        XCTAssertEqual(decryptedCorrect1, data)
        XCTAssertEqual(decryptedCorrect2, data)
    }
    
    func test4_pass_failure() {
        let data = "Hello, Crypto!".data(using: .utf8)!
        
        let encrypted = data.encrypted(password: "12345").shouldSucceed()!
        encrypted.decrypted(password: "1234").shouldFail()
    }
}
