import XCTest
import CryptoKit
import EssentialsTesting
@testable import Essentials

class CryptoDictFileID_Tests: XCTestCase {
    func test_Read_Save() {
        let tmpPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("test.txt").path
        let pass = "ThisIsTestMotherfucker"
        //print(tmpPath)
        
        let file = CryptoDictFileID(path: tmpPath, pass: pass)
        
        file.set(key: "temp1", value: "temp_value1").shouldSucceed()
        file.set(key: "temp2", value: "temp_value2").shouldSucceed()
        
        file.get(key: "temp1").shouldSucceed()
        file.get(key: "temp2").shouldSucceed()
        file.get(key: "temp3").shouldFail()
        
        file.set(key: "temp2", value: nil).shouldSucceed()
        file.get(key: "temp2").shouldFail()
        
        try! FileManager.default.removeItem(atPath: tmpPath)
    }
}
