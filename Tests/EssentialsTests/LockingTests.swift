import XCTest
import Essentials

final class LockingTests: XCTestCase {

    let iterations = 100000
    
//    var test_dic = [String:String]()
    var test_dic = LockedVar([String:String]())
    
    func testAccess() {
        let group = DispatchGroup()
        
        group.enter()
        
        DispatchQueue.global().async {
            for _ in 0..<self.iterations {
                self.test_dic[UUID().uuidString] = UUID().uuidString
            }
            
            group.leave()
        }
        
        DispatchQueue.global().async {
            for _ in 0..<self.iterations {
                self.test_dic[UUID().uuidString] = UUID().uuidString
            }
        }
        
        group.wait()
    }
}
