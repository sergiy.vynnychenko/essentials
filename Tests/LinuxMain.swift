import XCTest

import essentialsTests

var tests = [XCTestCaseEntry]()
tests += essentialsTests.allTests()
XCTMain(tests)
