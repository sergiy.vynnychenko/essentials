```swift
struct WTF : Error {
    var localizedDescription : String
    init(_ desc: String) {
        localizedDescription = desc
    }
}

func url(file: String, ext: String) -> Result<URL, Error> {
    if let fileUrl = Bundle.main.url(forResource: file, withExtension: ext) {
        return .success(fileUrl)
    } else {
        return .failure(WTF("can't create url"))
    }
}

extension Data {
    static func asResult(url: URL) -> Result<Data, Error> {
        do {
            return .success(try Data(contentsOf: url))
        } catch {
            return .failure(error)
        }
    }
}

extension Result {
    func dump() {
        switch self {
        case .success(let success):
            print("content: \(success)")
        case .failure(let error):
            print("ERROR: \(error.localizedDescription)")
        }
    }
    
    func onSuccess(_ block: (Success)->Void) {
        switch self {
        case .success(let success): block(success)
        default: break
        }
    }
    
    func onFailure(_ block: (Error)->Void) {
        switch self {
        case .failure(let error): block(error)
        default: break
        }
    }
}

url(file: "data1", ext: "")
    .flatMap { url in Data.asResult(url: url) }
    .map { data in String(decoding: data, as: UTF8.self) }
    .dump() // content: ಠ_ಠ

url(file: "data1", ext: "") // content: file:///var/folders/...
    .dump()

url(file: "bullshit", ext: "")  // ERROR: The operation couldn’t be completed.
    .dump()

private extension String {
    func removingLast() -> Self {
        var tmp = self
        tmp.removeLast()
        return tmp
    }
}

public extension Array {
    static func + (me: Self, newItem: Element) -> Self {
        var temp = me
        temp.append(newItem)
        return temp
    }
    
    func reduce<Value>(_ accum: Value, _ block: (Value, Element) -> Result<Value, Error>) -> Result<Value, Error> {
        var a = accum
        for item in self {
            switch block(a, item) {
            case .success(let success):
                a = success
            case .failure(let error):
                return .failure(error)
            }
        }
        return .success(a)
    }
    
    func flatMap<T>(_ transform: @escaping (Element) -> Result<T,Error>) -> Result<[T],Error> {
        return reduce([T]()) { array, next in
            return transform(next)
                .map { transformedNext in array + transformedNext }
        }
    }

/// we don't need aggregateResult, it can be replaced with flatMap { Result<T> } -> [T]
//
//    func aggregateResult<Value>() -> Result<[Value], Error> where Element == Result<Value, Error> {
//        flatMap { $0 }
//    }
}

["data1", "data2"]
    .flatMap { name in url(file: name, ext: "")                             }   // Result<[URL]>
    .flatMap { urls in urls.flatMap { Data.asResult(url: $0)              } }   // Result<[Data]>
    .map     { data in data.map     { String(decoding: $0, as: UTF8.self).removingLast() } }   // Result<[String]>
    .map     { $0.joined(separator: ", ") }
    .dump() // content: ಠ_ಠ, ಥ_ಥ

["data1", "data2", "bullshit"]
    .flatMap { name in url(file: name, ext: "")                             }   // Result<[URL]>
    .flatMap { urls in urls.flatMap { Data.asResult(url: $0)              } }   // Result<[Data]>
    .map     { data in data.map     { String(decoding: $0, as: UTF8.self).removingLast() } }   // Result<[String]>
    .map     { $0.joined(separator: ", ") }
    .dump() // ERROR: The operation couldn’t be completed.
```
