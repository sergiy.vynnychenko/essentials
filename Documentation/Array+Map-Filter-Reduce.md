# Map Filter Reduce for Array

```swift
// Lesson 1: map, flatMap, compactMap, filter, reduce

let arr = [1,2,3]

func transform(_ int: Int) -> [Int] {
  let params = [0,1,2]
  return params.map { int*10 + $0 }
}

// [[10, 11, 12], [20, 21, 22], [30, 31, 32]]
_ = arr.map { transform($0) }

// [10, 11, 12, 20, 21, 22, 30, 31, 32]
print( arr.flatMap { transform($0) } )
_    = arr.flatMap ( transform(_:) )    // same code

// [Value?] -> [Value]
// [1, 2, 3, nil] -> [1, 2, 3]
print( [1,2,3,nil].compactMap { $0 } )


///
/// FILTER
///

// [10, 11, 12]
print( [10, 11, 12, 20, 21, 22, 30, 31, 32].filter { $0 < 20 } )


///
/// REDUCE
///

// 6 = 1 + 2 + 3
print( [1,2,3].reduce(0,+) )
_    = [1,2,3].reduce(0) { $0 + $1 }
_    = [1,2,3].reduce(0) { accum, nextValue in accum + nextValue /* -> next accum */ }

// "🥷123" = "🥷" + "1" + "2" + "3"
print( [1,2,3].map { "\($0)" }.reduce("🥷",+) )




let nums = [1,2,3,4]
let evens = nums.filter { $0 % 2 == 0 }
let odds  = nums.filter { $0 % 2 == 1 }

print("filtered evens: \(evens)")  // [2, 4]
print("filtered odds: \(odds)")    // [1, 3]

class Numbers {
  var evens = [Int]()
  var odds  = [Int]()
  
  static func += (me: Numbers, int: Int) -> Numbers {
    if int % 2 == 0 {
      me.evens.append(int)
    } else {
      me.odds.append(int)
    }
    return me
  }
}

let n = [1,2,3,4]
  .reduce( Numbers(), += )

print("odds: \(n.odds)")    // [1, 3]
print("evens: \(n.evens)")  // [2, 4]

```
